﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using AMNsoftware;

public class AudioManager : SingletonPersistent<AudioManager> {


    AudioListener m_listener;
    public AudioMixer m_audioMixer;
    public bool musicEnabled = true;
    public bool sfxEnabled = true;

	// Use this for initialization
	void Start () {

        m_listener = GetComponent<AudioListener>();
        ChangeAudioMixerSnapshot("GameIntro");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateSoundOptions()
    {

        if (!musicEnabled)
        {

            m_audioMixer.SetFloat("MusicVolume", -80f);
        }
        else
        {

            m_audioMixer.SetFloat("MusicVolume", -5f);
        }

        if (!sfxEnabled)
        {
            float sfx = 0f;
            m_audioMixer.SetFloat("EffectsVolume",-80f);
            m_audioMixer.SetFloat("UIVolume", -80f);
        }
        else
        {
            m_audioMixer.SetFloat("UIVolume", 0f);
            m_audioMixer.SetFloat("EffectsVolume", -20f);
        }
    }

    public void ChangeAudioMixerSnapshot(string name)
    {

        var snapshot = m_audioMixer.FindSnapshot(name);
        if(snapshot == null)
        {

            Debug.Log("Could not find " + name);
        }
        else
        {

            AudioMixerSnapshot[] snaps = new AudioMixerSnapshot[1];
            float[] weights = new float[1];
            weights[0] = 1;
            snaps[0] = snapshot;
            m_audioMixer.TransitionToSnapshots(snaps, weights, 0.5f);

            UpdateSoundOptions();


        }


    }

    public void PlayClipOneShot(string clip,float duration = 0,float volume = 1, string mixerGroup = null)
    {
        GameObject audioClipObject = new GameObject();
        audioClipObject.name = "AudioClipObject(Generated)";
        AudioSourceObject source = audioClipObject.AddComponent<AudioSourceObject>();
        source.Init((AudioClip)Resources.Load("Audio/" + clip),duration, volume, mixerGroup);

    }

}
