﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlimitedLevel : MonoBehaviour {

    [SerializeField] Transform[] TrainSpawns;    [SerializeField] GameObject[] TrainCart;    [SerializeField] GameObject TrainMotor;    [SerializeField] GameObject[] m_cagePrefab;

    [SerializeField] SpriteRenderer backgroundImage;

    public int currentScore;
    public int poachersKilled;
    public int animalsSaved;

    public GameObject playerCharacter;
    public Animator introAnimator;    public Text introText;
    public PlayerController pc;
    float currentTrainLength;
    int lastTrack;
    float currentLevelSpeed;
    UnlimitedLevelData currentLevelData;
    public GameObject playerPivot;
    public GameObject track2;
    public GameObject track1;
    public float track1SwampPos;
    public float track1DefaultPos;
    // Use this for initialization
    void Start () {

        UIHandler.Instance.UpdateGemCount(MainGame.Instance.totalGems);
        UIHandler.Instance.UpdateLevelScore(MainGame.Instance.totalCoins, 0, 0);
        currentLevelData = DataManager.Instance.unlimitedLevelData[MainGame.Instance.unlimitedModeMenu.selectedLevel];
        backgroundImage.sprite = currentLevelData.levelBackground;
        currentLevelSpeed = currentLevelData.trainSpeed;
        pc.ChangeWeapon(MainGame.Instance.defaultWeapon);
        MainGame.Instance.ResetWeaponCoolDowns();
        MainGame.Instance.m_currentUnlimitedLevelSettings = this;
        introText.text = currentLevelData.name;
        introText.fontSize -= 12;
        introText.resizeTextMinSize = 30;
        introAnimator.Play("LevelIntro");
        MainGame.Instance.gameState = MainGame.GameStates.Level;

        Vector3 pv = Vector3.one;        if (DataManager.GetBool("PlayerIsLeft"))
        {

            playerPivot.transform.localScale = pv;
        }
        else
        {
            pv.x = -1;
            playerPivot.transform.localScale = pv;
        }

        if (currentLevelData.oneTrack)
        {
            Transform[] spawns = new Transform[1];
            spawns[0] = TrainSpawns[1];
            TrainSpawns = spawns;
            track2.SetActive(false);
            track1.transform.position = new Vector3(track1.transform.position.x, track1SwampPos, 0);
        }


        Timer.Instance.ResetTimer();
        Timer.Instance.StartTimer();
        StartCoroutine("TrainSpawner");

    }

    IEnumerator TrainSpawner()    {        var isFirst = true;        while (enabled)        {

            while (!MainGame.Instance.m_isPlaying)
            {

                yield return new WaitForEndOfFrame();
            }            GenerateTrain();            if (isFirst)
            {
                yield return new WaitForSeconds(4.5f);
                UIHandler.Instance.EnablePauseButton();
            }            if (currentLevelData.oneTrack)            {
                var time = (((currentTrainLength + 3) * 1.63f)) / (currentLevelSpeed - 0.1f);                if (isFirst)
                {
                    yield return new WaitForSeconds(time - 4.5f);
                }
                else
                {
                    yield return new WaitForSeconds(time);

                }
            }            else            {
                var time = (((currentTrainLength + 3) * 1.63f)) / (currentLevelSpeed - 0.1f) / 2;                if (isFirst)
                {
                    yield return new WaitForSeconds(time - 4.5f);
                }
                else
                {
                    yield return new WaitForSeconds(time);
                }            }            currentLevelSpeed += 0.1f;            isFirst = false;        }    }

    public void GenerateTrain()    {        GameObject NewTrain = new GameObject();        NewTrain.name = "TrainObject";        int track = Random.Range(0, TrainSpawns.Length);


        bool isNewTrack = false;        if (TrainSpawns.Length > 1)        {            while (!isNewTrack)            {                if (track == lastTrack)                {                    track = Random.Range(0, TrainSpawns.Length);                    isNewTrack = false;                }                else                {                    isNewTrack = true;                    lastTrack = track;                }            }        }        else        {            track = 0;        }        var numberOfCarts = Random.Range(3, 7);        var spawnPos = TrainSpawns[(int)track].position;        spawnPos.x -= 4;        NewTrain.transform.position = spawnPos;        currentTrainLength = numberOfCarts;        for (int i = 0; i < numberOfCarts; i++)        {
            GameObject cart;
            Vector3 pos;

            if (track == 1)
            {
                cart = (GameObject)Instantiate(TrainCart[0], NewTrain.transform.position, Quaternion.identity, NewTrain.transform);
            }
            else
            {
                cart = (GameObject)Instantiate(TrainCart[Random.Range(0, TrainCart.Length)], NewTrain.transform.position, Quaternion.identity, NewTrain.transform);
            }                pos = cart.transform.position;
                pos.x -= 1.63f * i;
                cart.transform.position = pos;
            var r = Random.Range(0, currentLevelData.m_animalsPrefab.Length);            if (cart.transform.childCount == 2)
            {
                GameObject cage;
                GameObject animal;

                if (Random.Range(0, 2) == 0)
                {

                    cage = Instantiate(m_cagePrefab[r], cart.transform.GetChild(0).transform.position, Quaternion.identity, cart.transform.GetChild(0).transform);
                    cage.transform.localPosition = new Vector3(0, 0.065f, 0);
                    animal = Instantiate(currentLevelData.m_animalsPrefab[r], cage.transform.position, Quaternion.identity, cage.transform);

                    if (track == 0)
                    {

                        cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                        cage.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        cage.GetComponentInChildren<SpriteRenderer>().sortingOrder = 3;
                        cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 1;

                        animal.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        animal.GetComponent<SpriteRenderer>().sortingOrder = 2;

                    }
                    else
                    {

                        cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                        cage.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        cage.GetComponentInChildren<SpriteRenderer>().sortingOrder = 3;
                        cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 1;

                        animal.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        animal.GetComponent<SpriteRenderer>().sortingOrder = 2;

                    }

                }
                else
                {
                    cage = Instantiate(currentLevelData.m_enemyPrefab[Random.Range(0, currentLevelData.m_enemyPrefab.Length)], cart.transform.GetChild(0).transform.position, Quaternion.identity, cart.transform.GetChild(0).transform);
                    cage.transform.localPosition = new Vector3(0, 0.25f, 0);

                    if (track == 0)
                    {

                        cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                        cage.GetComponent<Enemy>().SetSortingLayerName("Train1");


                    }
                    else
                    {

                        cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                        cage.GetComponent<Enemy>().SetSortingLayerName("Train2");


                    }
                }


            }
            else
            {

                var posss = cart.transform.GetChild(1).transform.position;
                var cage = Instantiate(m_cagePrefab[r], posss, Quaternion.identity, cart.transform.GetChild(1).transform);
                var animal = Instantiate(currentLevelData.m_animalsPrefab[r], cage.transform.position, Quaternion.identity, cage.transform);
                var enemy = Instantiate(currentLevelData.m_enemyPrefab[Random.Range(0, currentLevelData.m_enemyPrefab.Length)], cart.transform.GetChild(0).transform.position, Quaternion.identity, cart.transform.GetChild(0).transform);

                if (track == 0)
                {

                    cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                    cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                    cage.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                    cage.GetComponentInChildren<SpriteRenderer>().sortingOrder = 3;
                    cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                    cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 1;

                    animal.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                    animal.GetComponent<SpriteRenderer>().sortingOrder = 2;

                    enemy.GetComponent<Enemy>().SetSortingLayerName("Train1");
                    //enemy.GetComponent<SpriteRenderer>().sortingOrder = 4;

                }
                else
                {

                    cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                    cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                    cage.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                    cage.GetComponentInChildren<SpriteRenderer>().sortingOrder = 3;
                    cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                    cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 1;

                    animal.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                    animal.GetComponent<SpriteRenderer>().sortingOrder = 2;

                    enemy.GetComponent<Enemy>().SetSortingLayerName("Train2");



                }

            }



        }

        //Generate Motor
        var Motor = (GameObject)Instantiate(TrainMotor, NewTrain.transform.position, Quaternion.identity, NewTrain.transform);        if (track == 0)        {            Motor.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";            Motor.GetComponent<SpriteRenderer>().sortingOrder = 0;            Motor.GetComponent<TrainMotor>().Speed = currentLevelSpeed;        }        else        {            Motor.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";            Motor.GetComponent<SpriteRenderer>().sortingOrder = 0;            Motor.GetComponent<TrainMotor>().Speed = currentLevelSpeed;        }



        //1f + (0.1f * track * levelID) ;

    }

    public void ShowEndGame()
    {

        Timer.Instance.StopTimer();
        MainGame.Instance.m_isPlaying = false;
        UIHandler.Instance.DisablePauseButton();

        if(currentScore > DataManager.GetInt("UnlimitedLevelScore_" + MainGame.Instance.unlimitedModeMenu.selectedLevel))
        {
            Debug.Log("Saving : UnlimitedLevelScore_" + currentScore);
            DataManager.SetInt("UnlimitedLevelScore_" + MainGame.Instance.unlimitedModeMenu.selectedLevel, currentScore);
        }

        UIHandler.Instance.ShowEndUnlimitedGameUI(currentScore, currentLevelData,animalsSaved,poachersKilled);

    }

    public void Continue()    {        Timer.Instance.AddTime((int)(Timer.Instance.m_timeMax / 2f));        MainGame.Instance.m_isPlaying = true;        Timer.Instance.StartTimer();    }

    public void MissedPoacher()    {        Timer.Instance.DeductTime(2);    }

    public void AddScore(int score)    {        if (MainGame.Instance.m_isPlaying)        {            currentScore += score;            MainGame.Instance.totalCoins += score;            UIHandler.Instance.PlayTextAnimation("Coins");            UIHandler.Instance.UpdateLevelScore(MainGame.Instance.totalCoins, poachersKilled, animalsSaved);            TrophyHandler.Instance.UpdateTrophies(3, score);        }    }

}

[System.Serializable]
public class UnlimitedLevelData
{
    public string name;
    [SerializeField] public GameObject[] m_enemyPrefab;    [SerializeField] public GameObject[] m_animalsPrefab;
    public float trainSpeed;
    public Sprite levelBackground;
    public bool oneTrack;

}
