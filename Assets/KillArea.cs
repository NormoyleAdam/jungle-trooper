﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillArea : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Cage"))
        {
            collision.gameObject.GetComponent<Cage>().GetHit(1000);

        }
        else if (collision.CompareTag("Poacher"))
        {
            collision.gameObject.GetComponent<Enemy>().GetHit(1000);
        }

    }
}
