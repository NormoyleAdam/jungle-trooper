﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : MonoBehaviour {


    public Sprite happyAnimal;

    public void FreeAnimal()
    {

        GetComponent<SpriteRenderer>().sprite = happyAnimal;
    }
}
