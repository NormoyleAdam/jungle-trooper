﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AMNsoftware;

public class GorillaMode :  Singleton<GorillaMode>{

    public bool isEnabled;
    public float timeChangeAmount;

    [SerializeField] GameObject timerObject;
    [SerializeField] Text timerText;


    public void StartGorillaMode()
    {

        isEnabled = true;
        //overlay.enabled = true;
        timerObject.SetActive(true);
        StartCoroutine("TimerTick");
    }

    public void StopGorillaMode()
    {

        isEnabled = false;
        //overlay.enabled = false;
        timerObject.SetActive(false);
        Time.timeScale = 1f;
        StopAllCoroutines();
    }

    IEnumerator TimerTick()
    {

        var t = 0f;

        while (isEnabled)
        {

            while (MainGame.Instance.m_isPlaying)
            {

                t += 0.1f;

                timerText.text = ((int)10-(int)t).ToString() + "s";

                Time.timeScale = Mathf.Lerp(Time.timeScale, 0.5f, 0.1f);

                yield return new WaitForSecondsRealtime(0.1f);

                if(t > 10)
                {

                    StopGorillaMode();
                }

            }

            yield return new WaitForEndOfFrame();

        }

    }

}
