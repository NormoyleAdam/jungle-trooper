﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Init : MonoBehaviour {

	// Use this for initialization
	void Start () {


        UnityEngine.SceneManagement.SceneManager.sceneLoaded += LoadedGame;
        

    }

    void LoadedGame(UnityEngine.SceneManagement.Scene scene,UnityEngine.SceneManagement.LoadSceneMode mode)
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded -= LoadedGame;

    }
}
