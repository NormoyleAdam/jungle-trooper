﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndlessModeMenu : MonoBehaviour {

    public Image levelBackground;
    public Text levelName;
    public Text highScore;
    public int selectedLevel;

    public void Play()
    {

        MainGame.Instance.PlayLevel(selectedLevel);
        this.gameObject.SetActive(false);

    }

    public void OnEnable()
    {
        UpdateUI();
    }

    void UpdateUI()
    {

        levelBackground.sprite = DataManager.Instance.unlimitedLevelData[selectedLevel].levelBackground;
        levelName.text = DataManager.Instance.unlimitedLevelData[selectedLevel].name;
        highScore.text = DataManager.GetInt("UnlimitedLevelScore_" + selectedLevel).ToString();
        Debug.Log("Loading : UnlimitedLevelScore_" + DataManager.GetInt("UnlimitedLevelScore_" + selectedLevel).ToString());
        
    }

    public void NextLevel()
    {

        selectedLevel++;
        if (selectedLevel >= DataManager.Instance.unlimitedLevelData.Length)
            selectedLevel = 0;

        UpdateUI();
    }

    public void PreviousLevel()
    {

        selectedLevel--;
        if (selectedLevel < 0)
            selectedLevel = DataManager.Instance.unlimitedLevelData.Length-1;

        UpdateUI();
    }


}
