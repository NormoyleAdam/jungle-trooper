﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkeyBomb : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Explode()
    {

        var c = GetComponent<PolygonCollider2D>();
        c.enabled = true;

    }

    public void DisableTrigger()
    {

        var c = GetComponent<PolygonCollider2D>();
        c.enabled = false;
    }

    public void DestroyInstance()
    {
        Destroy(gameObject);
    }
}
