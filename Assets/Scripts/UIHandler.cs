﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEngine.UI;using AMNsoftware;public class UIHandler : Singleton<UIHandler>{    [SerializeField] Text levelScoreText;    [SerializeField] Text gemScoreText;    [SerializeField] Text poacherScoreText;    [SerializeField] Text animalScoreText;    [SerializeField] public GameObject m_splashUI;    [SerializeField] public GameObject m_mainMenuUI;    [SerializeField] public GameObject m_mainUI;    [SerializeField] public GameObject m_pauseMenuUI;    [SerializeField] public GameObject m_levelSelectUI;
    [SerializeField] public GameObject m_endlessModeMenuUI;    [SerializeField] Image m_blackBackground;    [SerializeField] public Button m_nextLevelButton;    [SerializeField] public Text m_poachersKilled;    [SerializeField] public Text m_animalsSaved;    [SerializeField] public Text m_coinsCollected;

    [SerializeField] public Animator m_mainMenuAnimator;    [SerializeField] Animator m_helpWindowAnimator;    [SerializeField] Animator m_trophiesWindowAnimator;    [SerializeField] Animator m_endGameWindowAnimator;    [SerializeField] Animator m_optionsWindowAnimator;    [SerializeField] Animator m_storeWindowAnimator;    [SerializeField] Animator m_blackBackgroundAnimator;
    [SerializeField] Animator m_endlessModeMenuAnimator;    [SerializeField] public Animator[] m_mainUIStars;    [SerializeField] public Image[] m_endGameStars;    [SerializeField] Text monkeyBombText;    [SerializeField] Text miniGunText;    [SerializeField] Text bananaPeelText;    [SerializeField] Text gorillaModeText;    [SerializeField] Animator levelUnlockedAnimator;
    [SerializeField] Animator levelCompleteAnimator;

    [SerializeField] GameObject m_pauseButton;
    [SerializeField] GameObject m_tutorialWindow;

    [SerializeField] Animator m_unlimitedLevelEndGameAnimator;
    [SerializeField] Text m_unlimitedLevelEndGameLevelName;
    [SerializeField] Text m_unlimitedLevelEndGameScore;
    [SerializeField] Text m_unlimitedLevelEndGameScoreAnimals;
    [SerializeField] Text m_unlimitedLevelEndGameScoreEnemies;
    [SerializeField] Text m_unlimitedLevelEndGameHighScore;
    [SerializeField] public Animator m_unlimitedLevelRetryWindowAnimator;
    [SerializeField] public Animator m_unlimitedLevelAdPopupWindowAnimator;
    [SerializeField] GameObject m_unlimitedLevelNewHighScore;
    [SerializeField] GameObject m_errorWindow;    [SerializeField] Text m_errorText;    [SerializeField] GameObject m_gameCompleteScreen;    public void ShowError(string reason)
    {
        m_errorText.text = reason;
        m_errorWindow.SetActive(true);
    }    public void ShowGameCompleteScreen()
    {

        m_gameCompleteScreen.SetActive(true);
    }    public void UpdateWeaponValues(int mb,int mg,int bp,int gm)    {        monkeyBombText.text = mb.ToString();        miniGunText.text = mg.ToString();        bananaPeelText.text = bp.ToString();        gorillaModeText.text = gm.ToString();    }    public void ShowEndUnlimitedGameUI(int score,UnlimitedLevelData data,int animals,int poachers)
    {
        m_unlimitedLevelEndGameAnimator.Play("GameOver");
        m_unlimitedLevelEndGameLevelName.text = data.name;
        m_unlimitedLevelEndGameScore.text = score.ToString();
        MainGame.Instance.lastCoinScore = score;

        m_unlimitedLevelEndGameScoreAnimals.text = animals.ToString();
        m_unlimitedLevelEndGameScoreEnemies.text = poachers.ToString();

        var high = DataManager.GetInt("UnlimitedLevelScore_" + MainGame.Instance.unlimitedModeMenu.selectedLevel.ToString());
        m_unlimitedLevelEndGameHighScore.text = high.ToString();

        if(score >= high)
        {
            DataManager.SetInt("UnlimitedLevelScore_" + MainGame.Instance.unlimitedModeMenu.selectedLevel.ToString(),score);
            m_unlimitedLevelNewHighScore.SetActive(true);
        }
        else
        {

            m_unlimitedLevelNewHighScore.SetActive(false);
        }

        MainGame.Instance.UpdateLevels();

    }    public void EnablePauseButton()
    {
        m_pauseButton.SetActive(true);
    }    public void DisablePauseButton()
    {

        m_pauseButton.SetActive(false);
    }    public void ShowTutorial()
    {
        FadeBlackBackground(true);
        m_tutorialWindow.SetActive(true);

    }    public void CloseTutorial()
    {
        FadeBlackBackground(false);
        m_tutorialWindow.SetActive(false);
    }    public void ShowEndGameUI(int p, int a, int c,int s,int level =-1)    {        m_endGameWindowAnimator.Play("GameOver");        m_poachersKilled.text = p.ToString();        m_animalsSaved.text = a.ToString();        m_coinsCollected.text = c.ToString();        MainGame.Instance.lastCoinScore = c;        MainGame.Instance.SaveData();        if (s > 0)
        {
            m_nextLevelButton.interactable = true;
            MainGame.Instance.UpdateLevels();
        }
        else
        {
            m_nextLevelButton.interactable = false;
        }        foreach(Image img in m_endGameStars)        {            img.color = new Color(0,0,0,0.5f);        }        for (int i = 0; i < s; i++)        {            m_endGameStars[i].color = Color.white;        }

        
        //FadeBlackBackground(true);
        MainGame.Instance.ResetWeaponCoolDowns();    }    public void UpdateGemCount(int g)    {        gemScoreText.text = g.ToString();    }        public void OpenOptionsWindow()
    {
        m_optionsWindowAnimator.Play("OptionsWindowOpen");
        AudioManager.Instance.PlayClipOneShot("Button");
        FadeBlackBackground(true);
    }

    public void CloseOptionsWindow()
    {

        m_optionsWindowAnimator.Play("OptionsWindowClose");
        AudioManager.Instance.PlayClipOneShot("Button");
        FadeBlackBackground(false);
    }    public void CloseEndGameWindow()    {        if (MainGame.Instance.isUnlimited)
        {
            m_unlimitedLevelEndGameAnimator.Play("EndGameOverLayClose");
        }
        else
        {
            m_endGameWindowAnimator.Play("EndGameOverLayClose");
        }        AudioManager.Instance.PlayClipOneShot("Button");
        //FadeBlackBackground(false);    }    public void PlayTextAnimation(string s)    {        switch (s)        {            case "Coins":                levelScoreText.GetComponent<Animator>().Play("UITextPulse");                break;            case "Gems":                gemScoreText.GetComponent<Animator>().Play("UITextPulse");                break;            case "Animals":                animalScoreText.GetComponent<Animator>().Play("UITextPulse");                break;            case "Enemies":                poacherScoreText.GetComponent<Animator>().Play("UITextPulse");                break;        }    }    public void UpdateLevelScore(int score,int poachers,int animals)    {        levelScoreText.text = score.ToString();        animalScoreText.text = animals.ToString();        poacherScoreText.text = poachers.ToString();    }    public void ShowLevelUnlocked()
    {

        levelUnlockedAnimator.Play("LevelUnlockedOverlay");

    }

    public void ShowLevelComplete()
    {

        levelCompleteAnimator.Play("LevelCompleteOverlay");

    }    public void ToggleHelpWindow()    {        AudioManager.Instance.PlayClipOneShot("Button");        m_helpWindowAnimator.SetBool("Open", !m_helpWindowAnimator.GetBool("Open"));        FadeBlackBackground(m_helpWindowAnimator.GetBool("Open"));    }    public void ToggleTrophiesWindow()    {        AudioManager.Instance.PlayClipOneShot("Button");        m_trophiesWindowAnimator.SetBool("Open", !m_trophiesWindowAnimator.GetBool("Open"));        FadeBlackBackground(m_trophiesWindowAnimator.GetBool("Open"));    }    public void ToggleOptionsWindow()    {        AudioManager.Instance.PlayClipOneShot("Button");        m_optionsWindowAnimator.SetBool("Open", !m_optionsWindowAnimator.GetBool("Open"));        FadeBlackBackground(m_optionsWindowAnimator.GetBool("Open"));    }    public void ToggleStoreWindow()    {        AudioManager.Instance.PlayClipOneShot("Button");        m_storeWindowAnimator.SetBool("Open", !m_storeWindowAnimator.GetBool("Open"));        FadeBlackBackground(m_storeWindowAnimator.GetBool("Open"));    }    void FadeBlackBackground(bool toBlack)    {        m_blackBackgroundAnimator.SetBool("Black",toBlack);    }}