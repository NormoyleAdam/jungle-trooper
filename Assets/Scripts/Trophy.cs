﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trophy : MonoBehaviour {

    [SerializeField] string name;
    [SerializeField] string info;
    [SerializeField] int currentLevel;
    [SerializeField] float currentValue;
    [SerializeField] int[] maxValue;

    [SerializeField] Text titleText;
    [SerializeField] Text infoText;
    [SerializeField] Text valueText;
    [SerializeField] Image progressBar;
    [SerializeField] Image[] stars;

    // Use this for initialization
    void Awake () {

        titleText.text = name;
        infoText.text = info.Replace("#",maxValue[currentLevel].ToString());
        valueText.text = (currentValue.ToString() + " / " + maxValue[currentLevel].ToString());
        progressBar.fillAmount = currentValue / maxValue[currentLevel];

	}
	
	public void UpdateProgress (float amount) {

        currentValue += amount;

        if(currentValue >= maxValue[currentLevel] && currentLevel < 3)
        {
            stars[currentLevel].color = Color.white;

            TrophyHandler.Instance.ShowPopup(name, info.Replace("#", maxValue[currentLevel].ToString()));
            currentLevel++;
            infoText.text = info.Replace("#", maxValue[currentLevel].ToString());
        }

        valueText.text = (currentValue.ToString() + " / " + maxValue[currentLevel].ToString());
        progressBar.fillAmount = (float)currentValue / maxValue[currentLevel];


    }
}
