﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotParticle : MonoBehaviour {

    ParticleSystem system;
    float t;
	// Use this for initialization
	void Start () {

        system = GetComponent<ParticleSystem>();


	}
	
	// Update is called once per frame
	void Update () {

        t += 1 * Time.deltaTime;
        if(t > system.main.duration)
        {

            Destroy(this.gameObject);

        }

	}
}
