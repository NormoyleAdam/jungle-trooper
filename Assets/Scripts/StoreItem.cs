﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreItem : MonoBehaviour {

    [Header("Item Cost")]
    public float m_costValue;
    public StoreItemType m_costType;
    public Text m_costText;

    [Header("Item Reward")]
    public int m_rewardValue;
    public StoreItemType m_rewardType;
    public Text m_rewardText;

    [Header("Item Image")]
    public Image m_itemImageComponent;
    public Sprite m_itemSprite;

    // Use this for initialization
    void Start () {

        var s = "";
        if (m_rewardValue > 1)
            s = "S";

        m_rewardText.text = m_rewardValue.ToString() + "x " + m_rewardType.ToString().ToUpper() + s;

        s = "";
        if (m_costValue > 1)
            s = "S";

        var cost = "";
        if(m_costType == StoreItemType.IAP_Cash)
        {
            //TODO
            //Auto detection of currency

            cost = "£ " + m_costValue.ToString();
        }
        else
        {
            cost = m_costValue.ToString() + "x " + m_costType.ToString().ToUpper() + s;

        }

        m_costText.text =  cost;
        m_itemImageComponent.sprite = m_itemSprite;
    }


    public void BuyItem()
    {
        StoreManager.Instance.MakePurchase(m_costValue, m_rewardType, m_rewardValue, m_costType);
    }

}
