﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainMotor : MonoBehaviour
{

    public float Speed;
    float length;
    Transform Train;
    bool smoke = true;
    AudioSource audioSource;
    // Use this for initialization
    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        Train = transform.parent;
        length = 1.65f * Train.childCount;

    }
	
    // Update is called once per frame
    void Update()
    {
		
        Train.position = Vector3.LerpUnclamped(Train.position, Train.position + Vector3.right, Speed * Time.deltaTime);

        if (Train.position.x > length + 1)
        {

            Destroy(Train.gameObject);

        } else if (smoke && Train.position.x > (2 / length) + 2.5f )
        {
            smoke =false;
            Destroy(transform.GetChild(0).gameObject);

        }

        var dist = length - Vector3.Distance(transform.position, Vector3.zero);

        audioSource.volume = Mathf.Clamp(dist / 10f, 0f, 0.6f);

    }
}
