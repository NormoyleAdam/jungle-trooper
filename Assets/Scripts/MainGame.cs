﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AMNsoftware;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class MainGame : SingletonPersistent<MainGame>
{

    public bool m_isPlaying = false;
    public int unlockedLevelID = 1;
    public int currentLevelID;
    public LevelSettings m_currentLevelSettings;
    public LevelSelection levelSelection;

    public GameObject adPopUpWindow;
    public GameObject retryPopUpWindow;

    public bool isUnlimited;
    public EndlessModeMenu unlimitedModeMenu;
    public UnlimitedLevel m_currentUnlimitedLevelSettings;
    public GameObject _shareMenu;

    public GameObject playAreaUI;
    public RectTransform playArea;

    public Sprite[] playerOutfits;

    int selectedlevelID;

    public int totalGems;
    public int totalCoins;
    public int monkeyBombAmount;
    public int slowMoAmount;
    public int lightningModeAmount;
    public int bananaPeelAmount;
    public bool isPaidVersion;

    public int lastCoinScore;

    public int[] levelScores;

    public WeaponSelectionItem defaultWeapon;
    // public GameObject[] weaponButtons;
    public WeaponSelectionItem[] weapons;

    public bool isPlayingLevel;
    public bool isPaused;
    public bool isLevelSelect;
    public bool isMainMenu;
    public bool playerIsOnLeft = true;

    public GameStates gameState = GameStates.Menu;

    public AudioClip starSFX;

    public GameObject rapidFireObject;

    public enum GameStates
    {
        Level,
        Menu,
        LevelSelect,
        Paused,
        EndGame

    }

    public void OnApplicationQuit()
    {
        SaveData();
    }

    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            if(gameState == GameStates.Level)
            {
                ShowPauseMenu();
            }
        }
    }

    void Start()
    {
        //StartCoroutine(SplashVideo());

        Application.runInBackground = true;

        playArea.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Screen.height / 3);

        GameStart();
    }

    // Use this for initialization
    public void GameStart()
    {
        //remove before build
        //DataManager.ResetData();


        //StartCoroutine("Splash");
        UIHandler.Instance.m_mainMenuUI.SetActive(true);

        if (!System.IO.File.Exists(Application.persistentDataPath + "/GameData.dmf"))
        {
            SaveData(true);
        }

        AudioManager.Instance.ChangeAudioMixerSnapshot("Menus");
        GetComponent<AudioSource>().Play();

        if (!DataManager.HasKey("Initialized"))
        {

            DataManager.SetString("Initialized", "true");
            DataManager.SetBool("PlayerIsLeft", playerIsOnLeft);
            FirstRun();

        }
        else
        {

            LoadSavedData();

        }

        UpdateWeaponValues();

    }

    IEnumerator SplashVideo()
    {
        yield return new WaitForSecondsRealtime(9);
        GameStart();
    }

    public void OpenShareMenu()
    {
        _shareMenu.SetActive(true);
    }

    public void TogglePlayerPosition(bool value)
    {
        playerIsOnLeft = !value;
        DataManager.SetBool("PlayerIsLeft", playerIsOnLeft);
        AudioManager.Instance.PlayClipOneShot("Button");
    }

    public void UpdateWeaponValues()
    {
        UIHandler.Instance.UpdateWeaponValues(monkeyBombAmount, lightningModeAmount, bananaPeelAmount, slowMoAmount);
    }

    public void ResetWeaponCoolDowns(bool isenergy = false)
    {

        foreach (WeaponSelectionItem w in weapons)
        {
            //Debug.LogWarning(w);

            if (isenergy)
            {
                if (w.weapon == WeaponType.Stars || w.weapon == WeaponType.Banana)
                {
                    w.CoolDownTimerReset();
                }
            }
            else
            {
                w.ResetWeapon();
                w.CoolDownTimerReset();
            }
        }
    }

    public void SaveData(bool isfirst = false)
    {
        if (isfirst)
        {

            DataManager.SetInt("unlockedLevelID", 1);
        }
        else
        {

            DataManager.SetInt("unlockedLevelID", DataManager.Instance.saveData.unlockedLevels);
        }

        DataManager.SetInt("totalGems", totalGems);
        DataManager.SetInt("totalCoins", totalCoins);

        DataManager.SetInt("monkeyBombAmount", monkeyBombAmount);
        DataManager.SetInt("minigunAmount", slowMoAmount);
        DataManager.SetInt("gorillaModeAmount", lightningModeAmount);
        DataManager.SetInt("bananaPeelAmount", bananaPeelAmount);

        for (int i = 0; i < levelScores.Length; i++)
        {
            DataManager.SetInt("Level" + i.ToString(), levelScores[i]);
        }

        DataManager.SetBool("isPaidVersion", isPaidVersion);

        DataManager.SaveGameData();
    }

    void LoadSavedData()
    {

        unlockedLevelID = DataManager.GetInt("unlockedLevelID");

        totalGems = DataManager.GetInt("totalGems");
        totalCoins = DataManager.GetInt("totalCoins");

        monkeyBombAmount = DataManager.GetInt("monkeyBombAmount");
        slowMoAmount = DataManager.GetInt("minigunAmount");
        lightningModeAmount = DataManager.GetInt("gorillaModeAmount");
        bananaPeelAmount = DataManager.GetInt("bananaPeelAmount");
        playerIsOnLeft = DataManager.GetBool("PlayerIsLeft");
        isPaidVersion = DataManager.GetBool("isPaidVersion");

        for (int i = 0; i < levelScores.Length; i++)
        {

            levelScores[i] = DataManager.GetInt("Level" + i.ToString());

        }

        if (isPaidVersion)
        {
            StoreManager.Instance.m_donationButton.SetActive(false);
        }
        else
        {
            StoreManager.Instance.m_donationButton.SetActive(true);
        }

    }

    public void FirstRun()
    {

        levelSelection.Init();

    }

    public void PlayGame()
    {
        UIHandler.Instance.m_mainMenuAnimator.Play("MenuGameSelect");
    }

    public void OpenLevelSelect()
    {

        levelSelection.Open();
        UIHandler.Instance.m_mainMenuUI.SetActive(false);
        gameState = GameStates.LevelSelect;
        AudioManager.Instance.PlayClipOneShot("UI_BaDa", 0, 1, "UI");
        isUnlimited = false;
    }

    public void OpenEndlessMode()
    {
        UIHandler.Instance.m_mainMenuUI.SetActive(false);
        UIHandler.Instance.m_endlessModeMenuUI.SetActive(true);
        isUnlimited = true;
        AudioManager.Instance.PlayClipOneShot("UI_BaDa", 0, 1, "UI");
    }

    public void ContinueLevel()
    {
        if (isUnlimited)
        {
            UIHandler.Instance.m_unlimitedLevelAdPopupWindowAnimator.Play("AdWindowPopUpClose");
            m_currentUnlimitedLevelSettings.Continue();
        }
        else
        {

            adPopUpWindow.GetComponent<Animator>().Play("AdWindowPopUpClose");
            m_currentLevelSettings.Continue();

        }

        UIHandler.Instance.CloseEndGameWindow();
        AudioManager.Instance.PlayClipOneShot("Button");
    }

    public void PlayNextLevel()
    {

        //unlockedLevelID = DataManager.Instance.data.unlockedLevels;
        //DataManager.GetData().levelConfigurations[DataManager.Instance.data.unlockedLevels].isUnlocked = true;
        //DataManager.SetInt("unlockedLevelID", DataManager.Instance.data.unlockedLevels);
        //Debug.Log("Unlocked Level : " + DataManager.Instance.data.unlockedLevels);

        DataManager.SaveGameData();
        UpdateLevels();

        if(currentLevelID >= 20)
            PlayLevel(20);
        else
            PlayLevel(currentLevelID + 1);


        AudioManager.Instance.PlayClipOneShot("Button");

    }

    public void ReplayLevel()
    {
        PlayLevel(currentLevelID);
        ClosePauseMenu();
    }

    public void PlayLevel(int value)
    {

        AudioManager.Instance.PlayClipOneShot("UI_BaDa", 0, 1, "UI");
        Time.timeScale = 1;
        SceneManager.sceneLoaded += LevelLoaded;

        if (isUnlimited)
        {

            SceneManager.LoadSceneAsync("UnlimitedLevel");
            selectedlevelID = value;
        }
        else
        {
            selectedlevelID = value;
            currentLevelID = selectedlevelID;
            SceneManager.LoadSceneAsync(selectedlevelID);
        }
    }

    public void LevelLoaded(Scene s, LoadSceneMode m)
    {

        SceneManager.sceneLoaded -= LevelLoaded;
        UIHandler.Instance.m_mainUI.SetActive(true);

        UIHandler.Instance.m_endlessModeMenuUI.SetActive(false);

        UIHandler.Instance.m_levelSelectUI.SetActive(false);

        foreach (Animator a in UIHandler.Instance.m_mainUIStars)
        {
            a.Play("New State");
            a.gameObject.GetComponent<Image>().enabled = false;
        }

        m_isPlaying = true;
        gameState = GameStates.Level;
        AudioManager.Instance.ChangeAudioMixerSnapshot("Level");
        playArea.gameObject.SetActive(true);


    }

    public void BackToLevelSelection()
    {

        SceneManager.sceneUnloaded += BackToLevelSelectComplete;
        SceneManager.LoadScene(0);
        playArea.gameObject.SetActive(false);
    }

    public void BackToLevelSelectComplete(Scene s)
    {
        SceneManager.sceneUnloaded -= BackToLevelSelectComplete;

        AudioManager.Instance.ChangeAudioMixerSnapshot("Menus");
        UIHandler.Instance.m_mainUI.SetActive(false);

        if (isUnlimited)
        {

            gameState = GameStates.LevelSelect;
            UIHandler.Instance.m_endlessModeMenuUI.SetActive(true);
        }
        else
        {

            levelSelection.levelButtons[currentLevelID - 1].GetComponent<LevelSelectButton>().UpdateStars(DataManager.Instance.saveData.scoreData[currentLevelID - 1].stars);
            gameState = GameStates.LevelSelect;
            // Debug.Log(DataManager.GetInt("unlockedLevelID").ToString() + " / " + currentLevelID.ToString());
            levelSelection.Open();

        }
    }

    public void PickUpItem(ItemType e)
    {
        switch (e)
        {

            case ItemType.Gem:
                totalGems++;
                UIHandler.Instance.UpdateGemCount(totalGems);
                StoreManager.Instance.m_gemsText.text = totalGems.ToString();
                UIHandler.Instance.PlayTextAnimation("Gems");
                TrophyHandler.Instance.UpdateTrophies(5);
                break;
            case ItemType.Bomb:
                monkeyBombAmount++;
                break;
            case ItemType.GorillaMode:           
                slowMoAmount++;
                break;
            case ItemType.BananaPeel:
                bananaPeelAmount += 5;
                break;
            case ItemType.MiniGun:
                lightningModeAmount++;
                break;
            case ItemType.RapidFire:
                StartCoroutine("RapidFire");
                break;
        }

        AudioManager.Instance.PlayClipOneShot("UI_Pop", 0, 1, "UI");

        UpdateWeaponValues();

    }

    public void ShowAdProbably()
    {

        if (UnityEngine.Random.Range(0, 3) == 1)
        {
            if (isUnlimited)
            {
                UIHandler.Instance.m_unlimitedLevelAdPopupWindowAnimator.Play("AdWindowPopup");
            }
            else
            {
                adPopUpWindow.GetComponent<Animator>().Play("AdWindowPopup");
            }
        }
        else
        {
            if (isUnlimited)
            {

                AdsManager.Instance.ShowAd();
                UIHandler.Instance.m_unlimitedLevelRetryWindowAnimator.Play("RetryWindowPopup");

            }
            else
            {

                AdsManager.Instance.ShowAd();
                retryPopUpWindow.GetComponent<Animator>().Play("RetryWindowPopup");

            }

        }

    }

    public void ShowPauseMenu()
    {
        UIHandler.Instance.m_pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        gameState = GameStates.Paused;
        AudioManager.Instance.ChangeAudioMixerSnapshot("Menus");
        AudioManager.Instance.PlayClipOneShot("Button");
    }

    public void ClosePauseMenu()
    {
        if (UIHandler.Instance.m_pauseMenuUI.activeSelf)
        {
            UIHandler.Instance.m_pauseMenuUI.SetActive(false);
            Time.timeScale = 1;
            gameState = GameStates.Level;
            AudioManager.Instance.ChangeAudioMixerSnapshot("Level");

        }

        //AudioManager.Instance.PlayClipOneShot("Button");
    }

    public void UpdateLevels()
    {
        if (m_currentLevelSettings != null)
        {
            if (m_currentLevelSettings.starsObtained > 0)
            {

                if (DataManager.Instance.saveData.unlockedLevels <= m_currentLevelSettings.levelID )
                {
                    Debug.Log(m_currentLevelSettings.levelID);
                    levelSelection.UnlockNextLevel();
                }
                else
                {

                    Debug.Log("Level ID less than LevelUnlocked");
                }

            }
            else
            {
                Debug.Log("Level not passed");
            }
        }
        else
        {
            Debug.Log("Level Settings dont exist");
        }

    }

    public void ResumeGame()
    {
        ClosePauseMenu();
    }

    public void backtomenufromgame()
    {

        levelSelection.levelButtons[currentLevelID - 1].GetComponent<LevelSelectButton>().UpdateStars(DataManager.Instance.saveData.scoreData[currentLevelID - 1].stars);
        BackToMenu();
    }

    public void BackToMenu()
    {
        UIHandler.Instance.m_pauseMenuUI.SetActive(false);
        Time.timeScale = 1;
        gameState = GameStates.Menu;
        UIHandler.Instance.m_levelSelectUI.SetActive(false);
        UIHandler.Instance.m_mainMenuUI.SetActive(true);
        UIHandler.Instance.m_mainMenuAnimator.Play("MenuEnter");
        AudioManager.Instance.ChangeAudioMixerSnapshot("Menus");
        UIHandler.Instance.m_endlessModeMenuUI.SetActive(false);
        UIHandler.Instance.m_mainUI.SetActive(false);
        playArea.gameObject.SetActive(false);
        if (isPaidVersion)
        {
            StoreManager.Instance.m_donationButton.SetActive(false);
        }
        else
        {
            StoreManager.Instance.m_donationButton.SetActive(true);
        }

        SceneManager.LoadSceneAsync("Game");


    }

    // Update is called once per frame
    void Update()
    {

        UpdateWeaponValues();


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            AudioManager.Instance.PlayClipOneShot("Button");
            switch (gameState)
            {

                case GameStates.Menu:
                    ShowExitPopup();
                    break;
                case GameStates.Level:
                    ShowPauseMenu();
                    break;
                case GameStates.LevelSelect:
                    BackToMenu();
                    break;
                case GameStates.Paused:
                    ResumeGame();
                    break;


            }

        }
    }

    public IEnumerator RapidFire()
    {
        float t = 0;
        rapidFireObject.SetActive(true);
        while (t < 5)
        {
            t += 0.5f;
            yield return new WaitForSeconds(0.5f);
            foreach (var item in weapons)
            {
                item.CoolDownTimerReset();
            }
        }

        rapidFireObject.SetActive(false);
    }

    [SerializeField] GameObject exitPopupWindow;

    void ShowExitPopup()
    {
        exitPopupWindow.SetActive(true);
    }

    public void CloseExitPopUp()
    {
        exitPopupWindow.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}

[System.Serializable]
public class LevelConfig
{
    [SerializeField] public string levelName;
    [SerializeField] public int levelID;
    [SerializeField] public int[] animalsSavedStarRequirements;
    [SerializeField] public int[] poachersKilledStarRequirements;
    [SerializeField] public float trainSpeed;
    [SerializeField] public float timeTilSpawn;
    [SerializeField] public int maxTrainLength;
    [SerializeField] public int minTrainLength;

}


[System.Serializable]
public class LevelScoreData
{
    [SerializeField] public int stars;
    [SerializeField] public int animalsSaved;
    [SerializeField] public int poachersKilled;

    internal void UpdateScores(int starsObtained, int animalsSaved, int poachersKilled)
    {
        this.stars = starsObtained;
        this.animalsSaved = animalsSaved;
        this.poachersKilled = poachersKilled;
    }
}
