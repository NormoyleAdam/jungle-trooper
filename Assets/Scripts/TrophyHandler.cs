﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AMNsoftware;

public class TrophyHandler : Singleton<TrophyHandler> {

    [SerializeField] Trophy[] m_trophies;
    [SerializeField] Text popupInfo;
    [SerializeField] Animator popupAnimator;

    public void UpdateTrophies(int id,float amount = 1)
    {
        m_trophies[id].UpdateProgress(amount);
    }

    public void ShowPopup(string title,string info)
    {

        popupInfo.text = title;
        popupAnimator.Play("ShowTrophyPopup");

    }
}

