﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectButton : MonoBehaviour {

    public int levelNumber;
    public int starScore;
    public bool isUnlocked;

    [Header ("Components")]
    public Text levelText;
    public Image[] starImages;
    public GameObject enemy;
    public GameObject animal;


    // Use this for initialization

    void Awake () {

        foreach (Image i in starImages)
        {
            i.enabled = false;
        }

        if (!isUnlocked)
        {

            GetComponent<Transform>().localScale = new Vector3(0, 0, 1);
        }
        else
        {

            GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        }

        UpdateStars(MainGame.Instance.levelScores[levelNumber-1]);

        levelText.text = levelNumber.ToString();

    }

    public void UnlockLevel()
    {
        isUnlocked = true;
        GetComponent<Animator>().SetBool("Unlocked", isUnlocked);
    }

    public void PlayLevel()
    {

        LevelSelection.Instance.OpenInfoPanel(levelNumber);

    }
	
    public void UpdateStars(int s)
    {

        GetComponent<Animator>().SetBool("Unlocked", isUnlocked);

        if (starScore < s)
        {
            starScore = s;

            for (int i = 0; i < starScore; i++)
            {

                starImages[i].enabled = true;

            }
        }

        if(starScore == 3)
        {
            enemy.SetActive(false);
            animal.SetActive(true);

        }
        else
        {
            enemy.SetActive(true);
            animal.SetActive(false);
        }
    }

}
