﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using AMNsoftware;
using System;

public class StoreManager : SingletonPersistent<StoreManager>, IStoreListener {


    [SerializeField] GameObject m_confirmationWindow;
    [SerializeField] Text m_confirmationWindowText;
    [SerializeField] GameObject m_purchaseFinishedWindow;
    [SerializeField] Text m_purchaseFinishedText;
    [SerializeField] public GameObject m_donationButton;
    [SerializeField] GameObject m_donationWindow;
    [SerializeField] GameObject donationCompleteScreen;

    public GameObject[] m_catagories;
    public Toggle[] m_catagoriesTabs;

    ActivePurchase currentActivePurchase;

    public Text m_coinsText;
    public Text m_gemsText;

    public static string kProductIDPrefix = "com.adotgames.jungletrooper.";

    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.


    // Use this for initialization
    void Start () {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        builder.AddProduct(kProductIDPrefix + "gem1", ProductType.Consumable,new IDs(){ {(kProductIDPrefix + "gem1"),GooglePlay.Name,AppleAppStore.Name} });
        builder.AddProduct(kProductIDPrefix + "gem3", ProductType.Consumable, new IDs() { { (kProductIDPrefix + "gem3"), GooglePlay.Name, AppleAppStore.Name } });
        builder.AddProduct(kProductIDPrefix + "gem5", ProductType.Consumable, new IDs() { { (kProductIDPrefix + "gem5"), GooglePlay.Name, AppleAppStore.Name } });
        builder.AddProduct(kProductIDPrefix + "gem10", ProductType.Consumable, new IDs() { { (kProductIDPrefix + "gem10"), GooglePlay.Name, AppleAppStore.Name } });
        // Continue adding the non-consumable product.
        builder.AddProduct(kProductIDPrefix + "ad_removal", ProductType.NonConsumable, new IDs() { { (kProductIDPrefix + "ad_removal"), GooglePlay.Name, AppleAppStore.Name } });
        // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
        // if the Product ID was configured differently between Apple and Google stores. Also note that
        // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
        // must only be referenced here. 

        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyConsumable(string id)
    {
        // Buy the consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID(kProductIDPrefix + id);
    }

    void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        // A consumable product has been purchased by this user.
        if (String.Equals(args.purchasedProduct.definition.id, kProductIDPrefix + "gem1", StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            //5 Gems
            CashPurchaseComplete("gem1");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDPrefix + "gem3", StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            //20 Gems
            CashPurchaseComplete("gem3");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDPrefix + "gem5", StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            //75 Gems
            CashPurchaseComplete("gem5");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDPrefix + "gem10", StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            //250 Gems
            CashPurchaseComplete("gem10");

        }


        if (String.Equals(args.purchasedProduct.definition.id, kProductIDPrefix + "ad_removal", StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            MainGame.Instance.isPaidVersion = true;
            m_donationButton.SetActive(false);
            CashPurchaseComplete("donation");
        }




        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        m_purchaseFinishedWindow.SetActive(true);
        m_purchaseFinishedText.text = "Purchase Complete";


        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        m_purchaseFinishedWindow.SetActive(true);
        m_purchaseFinishedText.text = failureReason.ToString();

        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    //============================================================

    public void ChangeCatagory(int cat)
    {

        CloseAllCatagories();
        m_catagories[cat].SetActive(true);
        m_catagoriesTabs[cat].isOn = true;
    }

    void CloseAllCatagories()
    {

        for (int i = 0; i < m_catagories.Length; i++)
        {
            m_catagories[i].SetActive(false);
            m_catagoriesTabs[i].isOn = false;
        }

    }

    public void MakePurchase(float cost, StoreItemType buyItemType, int amount, StoreItemType costItemType)
    {
        switch (costItemType)
        {
            case StoreItemType.Coin:
                if (MainGame.Instance.totalCoins < cost)
                {
                    m_purchaseFinishedWindow.SetActive(true);
                    m_purchaseFinishedText.text = "Purchase Failed";


                    return;
                }
                break;
            case StoreItemType.Gem:
                if (MainGame.Instance.totalGems < cost)
                {

                    m_purchaseFinishedWindow.SetActive(true);
                    m_purchaseFinishedText.text = "Purchase Failed";

                    return;
                }
                break;
        }


        m_confirmationWindow.SetActive(true);
        m_confirmationWindowText.text = "Are you sure you want to buy " + amount + " " + buyItemType.ToString() + " for " + cost + " " + costItemType.ToString();

        currentActivePurchase = new ActivePurchase();
        currentActivePurchase.cost = cost;
        currentActivePurchase.amount = amount;
        currentActivePurchase.buyItemType = buyItemType;
        currentActivePurchase.costItemType = costItemType;

    }

    public void ConfirmPurchase()
    {
        switch (currentActivePurchase.costItemType)
        {
            case StoreItemType.Coin:
                if (MainGame.Instance.totalCoins >= currentActivePurchase.cost)
                {
                    TrophyHandler.Instance.UpdateTrophies(4, currentActivePurchase.cost);
                    MainGame.Instance.totalCoins -= (int)currentActivePurchase.cost;
                    break;
                }
                return;
            case StoreItemType.Gem:
                if (MainGame.Instance.totalGems >= currentActivePurchase.cost)
                {
                    MainGame.Instance.totalGems -= (int)currentActivePurchase.cost;
                    break;
                }
                return;
            case StoreItemType.PlayerSkin:
                return;
            case StoreItemType.EnergyBoost:
                return;
            case StoreItemType.BannaBomb:
                return;
            case StoreItemType.SloMode:
                return;
            case StoreItemType.BananaPeel:
                return;
            case StoreItemType.IAP_Cash:
                BuyConsumable(currentActivePurchase.buyItemType.ToString().ToLower() + currentActivePurchase.amount.ToString());
                return;
        }

        switch (currentActivePurchase.buyItemType)
        {
            case StoreItemType.Coin:
                MainGame.Instance.totalCoins += currentActivePurchase.amount;
                m_purchaseFinishedWindow.SetActive(true);
                m_purchaseFinishedText.text = "Purchase Complete";

                break;
            case StoreItemType.Gem:
                MainGame.Instance.totalGems += currentActivePurchase.amount;
                break;
            case StoreItemType.PlayerSkin:
                DataManager.SetInt("CurrentPlayerSkin", currentActivePurchase.amount);
                m_purchaseFinishedWindow.SetActive(true);
                m_purchaseFinishedText.text = "Purchase Complete";

                break;
            case StoreItemType.EnergyBoost:
                MainGame.Instance.lightningModeAmount += currentActivePurchase.amount;
                m_purchaseFinishedWindow.SetActive(true);
                m_purchaseFinishedText.text = "Purchase Complete";

                break;
            case StoreItemType.BannaBomb:
                MainGame.Instance.monkeyBombAmount += currentActivePurchase.amount;
                m_purchaseFinishedWindow.SetActive(true);
                m_purchaseFinishedText.text = "Purchase Complete";

                break;
            case StoreItemType.SloMode:
                MainGame.Instance.slowMoAmount += currentActivePurchase.amount;
                m_purchaseFinishedWindow.SetActive(true);
                m_purchaseFinishedText.text = "Purchase Complete";

                break;
            case StoreItemType.BananaPeel:
                MainGame.Instance.bananaPeelAmount += currentActivePurchase.amount;
                m_purchaseFinishedWindow.SetActive(true);
                m_purchaseFinishedText.text = "Purchase Complete";

                break;
            case StoreItemType.IAP_Cash:
                break;
        }

        UpdateUI();
        currentActivePurchase = null;
    }


    void CashPurchaseComplete(string id)
    {
        switch (id)
        {
            case "gem1":
                MainGame.Instance.totalGems += currentActivePurchase.amount;
                break;
            case "gem3":
                MainGame.Instance.totalGems += currentActivePurchase.amount;
                break;
            case "gem5":
                MainGame.Instance.totalGems += currentActivePurchase.amount;
                break;
            case "gem10":
                MainGame.Instance.totalGems += currentActivePurchase.amount;
                break;
            case "donation":
                MainGame.Instance.totalGems += 3;
                MainGame.Instance.monkeyBombAmount += 2;
                MainGame.Instance.slowMoAmount += 5;
                MainGame.Instance.UpdateWeaponValues();
                UIHandler.Instance.UpdateGemCount(MainGame.Instance.totalGems);
                donationCompleteScreen.SetActive(true);
                break;
        }

        Debug.Log("CashPurchaseComplete");

    }

    public void RemoveAds()
    {
        BuyProductID(kProductIDPrefix + "ad_removal");

    }

    public void Update()
    {
        UpdateUI();
    }

    public void OpenStore()
    {
        ChangeCatagory(0);
        UpdateUI();
        UIHandler.Instance.ToggleStoreWindow();
    }

    void UpdateUI()
    {
        m_coinsText.text = MainGame.Instance.totalCoins.ToString();
        m_gemsText.text = MainGame.Instance.totalGems.ToString();
    }

    public void CloseStore()
    {

        UIHandler.Instance.ToggleStoreWindow();
    }

}

public enum StoreItemType
{
    Coin,
    Gem,
    PlayerSkin,
    EnergyBoost,
    BannaBomb,
    SloMode,
    BananaPeel,
    IAP_Cash

}

public class ActivePurchase
{
    public ActivePurchase(){}

    public float cost;
    public StoreItemType buyItemType;

    public int amount;
    public StoreItemType costItemType;
}

