﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AMNsoftware;

public class Timer : Singleton<Timer>
{

    [SerializeField] bool m_isEnabled;
    public float m_timerValue;
    public float m_timeMax;

    public GameObject m_timerIntroUI;
    public Text m_timerIntroText;

    float m_gracePeriod;
    bool m_gracePeriodOver;

    [SerializeField] Image m_barImage;


    // Use this for initialization
    void Start()
    {
        ResetTimer();
    }

    public void ResetTimer()
    {
        m_timerValue = m_timeMax;
        UpdateUI();
    }

    public void StopTimer()
    {
        m_isEnabled = false;
        StopCoroutine("TimerTick");
        StopCoroutine("TimerIntro");
        

    }


    public void StartTimer()
    {
        ResetTimer();
       
        m_isEnabled = true;
        StartCoroutine("TimerIntro");

    }

    public void AddTime(int t)
    {
        //if (m_timerValue > 0 && !m_isEnabled)
        //{
            
        //    StartTimer();

        //}

        m_timerValue = Mathf.Min(m_timerValue + t, m_timeMax);
        UpdateUI();


    }

    public void DeductTime(float t)
    {
        //if (m_timerValue > 0 && !m_isEnabled)
        //{
        //    StartTimer();

        //}

        if (m_gracePeriodOver && MainGame.Instance.m_isPlaying)
        {
            m_timerValue -= t;
        }

        UpdateUI();
    }

    void UpdateUI()
    {

        Instance.m_barImage.fillAmount = (float)((m_timerValue / m_timeMax));


        if (Instance.m_barImage.fillAmount <= 0 && m_isEnabled)
        {

            m_isEnabled = false;
            StopTimer();

            if (MainGame.Instance.isUnlimited)
            {
                MainGame.Instance.m_currentUnlimitedLevelSettings.ShowEndGame();
            }
            else
            {
                MainGame.Instance.m_currentLevelSettings.ShowEndGame();
            }
        }

    }

    IEnumerator TimerTick()
    {
        m_isEnabled = true;
        while (Instance.m_isEnabled)
        {

            while (MainGame.Instance.m_isPlaying)
            {

                m_timerValue -= 1 * Time.deltaTime;

                UpdateUI();

                yield return new WaitForEndOfFrame();


            }

            yield return new WaitForEndOfFrame();

        }

    }

    IEnumerator TimerIntro()
    {
        m_gracePeriodOver = false;
        yield return new WaitForSecondsRealtime(1);
        m_timerIntroUI.SetActive(true);
        m_timerIntroText.text = "3";
        yield return new WaitForSecondsRealtime(1);
        m_timerIntroText.text = "2";
        yield return new WaitForSecondsRealtime(1);
        m_timerIntroText.text = "1";
        yield return new WaitForSecondsRealtime(1);
        m_timerIntroText.text = "GO";
        yield return new WaitForSecondsRealtime(0.5f);
        m_timerIntroUI.SetActive(false);
        MainGame.Instance.m_isPlaying = true;
        StartCoroutine("TimerTick");
        yield return new WaitForSecondsRealtime(3);
        m_gracePeriodOver = true;
    }

    // Update is called once per frame
    void Update()
    {
        // UpdateUI();

    }
}
