﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour {


    public bool isDevelopmentBuild;
    public bool adsEnabled = true;
    
    [Range(0,100)]
    public float masterVolume;
    [Range(0, 100)]
    public float musicVolume;
    [Range(0, 100)]
    public float sfxVolume;

    public void ToggleMusic(bool value)
    {
        AudioManager.Instance.musicEnabled = !value;
        AudioManager.Instance.UpdateSoundOptions();
    }
    public void ToggleSFX(bool value)
    {
        AudioManager.Instance.sfxEnabled = !value;
        AudioManager.Instance.UpdateSoundOptions();
    }
}
