﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ApplyMaterials : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    [MenuItem("Tools/Optimization/Null Materials On Sprites And UI")]
    static void FixNullMaterials()
    {

        var i = 0;
        var renderers = Resources.FindObjectsOfTypeAll(typeof(SpriteRenderer)) as SpriteRenderer[];

        foreach (var item in renderers)
        {

            i++;
            item.material = (Material)Resources.Load("DefaultSpriteMaterial");
        }

        var images = Resources.FindObjectsOfTypeAll(typeof(Image)) as Image[];
        foreach (var item in images)
        {

            i++;
            item.material = (Material)Resources.Load("DefaultUIMaterial");
        }

        Debug.Log("Null Material Optimization Complete!");
        Debug.Log("Fixed " + i.ToString() + " Null Materials");
    }

    [MenuItem("Tools/Optimization/Remove UI Effects")]
    static void RemoveAllUIEffects()
    {

        var renderers = Resources.FindObjectsOfTypeAll(typeof(Outline)) as Outline[];

        foreach (var item in renderers)
        {
            DestroyImmediate(item);
            
        }

        var shadow = Resources.FindObjectsOfTypeAll(typeof(Shadow)) as Shadow[];

        foreach (var item in shadow)
        {
            DestroyImmediate(item);

        }

    }


    }
