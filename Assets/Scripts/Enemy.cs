﻿using System.Collections;using System.Collections.Generic;using UnityEngine;public class Enemy : MonoBehaviour{    public int m_enemyType;    public float m_health;    public int m_reward;    public int m_timeReward;    public int m_dropProbabillity;    public GameObject[] m_dropItems;    public AudioClip[] hitFSX;    string sortingLayer;
    [SerializeField] GameObject m_splatEffect;    AudioSource audioSource;    Vector3 origin;    Vector3 pos;    bool isMoving;    // Use this for initialization    void Start()    {        audioSource = gameObject.AddComponent<AudioSource>();        isMoving = (MainGame.Instance.currentLevelID > 13) ? true : false;        origin = transform.localPosition;        pos = origin;        isleft = true;        if (isMoving)            StartCoroutine("Movement");    }    public void SetSortingLayerName(string layer)    {        var sprites = GetComponentsInChildren<SpriteRenderer>();        foreach (var item in sprites)        {            item.sortingLayerName = layer;        }        sortingLayer = layer;    }    public void GetHit(float d)    {        if (MainGame.Instance.m_isPlaying)        {            m_health -= d;            audioSource.PlayOneShot(hitFSX[Random.Range(0, 3)], 0.5f);            if (m_health <= 0)                Kill();        }    }    private void OnTriggerEnter2D(Collider2D collision)    {        if(collision.tag == "MonkeyBomb")        {            Instantiate(m_splatEffect, transform.position, Quaternion.identity);            GetHit(1);
            AudioManager.Instance.PlayClipOneShot("Splat");        }    }    void Kill()    {
        if (!MainGame.Instance.isUnlimited)
        {
            MainGame.Instance.m_currentLevelSettings.poachersKilled++;
            MainGame.Instance.m_currentLevelSettings.AddScore(m_reward);
        }
        else
        {
            MainGame.Instance.m_currentUnlimitedLevelSettings.poachersKilled++;
            MainGame.Instance.m_currentUnlimitedLevelSettings.AddScore(m_reward);
        }        Timer.Instance.AddTime(m_timeReward);        UIHandler.Instance.PlayTextAnimation("Enemies");        GetComponent<EdgeCollider2D>().enabled = false;        TrophyHandler.Instance.UpdateTrophies(1);        if (CalculateDropProbablillity())            DropItem();        GetComponent<Animator>().SetBool("Dead", true);    }    bool CalculateDropProbablillity()    {        int i = Random.Range(1, 101);        return i <= m_dropProbabillity;    }    void DropItem()    {        int i = Random.Range(0, m_dropItems.Length);        var DroppedItem = Instantiate(m_dropItems[i], transform.position, Quaternion.identity);        DroppedItem.GetComponent<SpriteRenderer>().sortingLayerName = sortingLayer;        DroppedItem.GetComponent<SpriteRenderer>().sortingOrder = 7;        DroppedItem.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = sortingLayer;        DroppedItem.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 6;        //DroppedItem.GetComponent<Rigidbody2D>().AddForce(Vector2.up);    }    void Update()    {        if (transform.position.x > 1.2f && m_health > 0)        {            if (!MainGame.Instance.isUnlimited)
            {
                MainGame.Instance.m_currentLevelSettings.MissedPoacher();
            }
            else
            {

                MainGame.Instance.m_currentUnlimitedLevelSettings.MissedPoacher();
            }            m_health = 0;        }    }    bool isleft;    public IEnumerator Movement()
    {

        while (MainGame.Instance.m_isPlaying)
        {
            pos = transform.localPosition;

            if (pos.x < origin.x - 0.2f)
            {
                isleft = false;
            }
            else if(pos.x > origin.x + 0.2f)
            {
                isleft = true;
            }

            if (isleft)
            {

                pos.x -= 0.2f * Time.deltaTime;

            }
            else
            {

                pos.x += 0.2f * Time.deltaTime;
            }

            transform.localPosition = pos;
            yield return new WaitForEndOfFrame();
        }
    }}    