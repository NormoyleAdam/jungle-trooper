﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banana : MonoBehaviour
{

    [SerializeField] GameObject m_coinsEffect;
    [SerializeField] GameObject m_dustEffect;
    public bool isBanana;


    void OnTriggerEnter2D(Collider2D Col)
    {
        
        if (Col.gameObject.CompareTag("Poacher"))
        {
            Debug.Log(Col.gameObject.name);
            Col.gameObject.GetComponent<Enemy>().GetHit(1);
            Instantiate(m_dustEffect, transform.position, Quaternion.identity);
            

            if (isBanana)
            {
                AudioManager.Instance.PlayClipOneShot("Splat");
                Instantiate(m_coinsEffect, transform.position, Quaternion.identity);

                Col.transform.parent.SetAsLastSibling();
                if (Col.transform.parent.parent.GetSiblingIndex() > 0)
                {
                    Debug.Log(Col.transform.parent.parent.name);
                    Debug.Log(Col.transform.parent.parent.GetChild(0).name);

                    if (Col.transform.parent.parent.GetChild(0).childCount > 0)
                    {
                        Debug.Log(Col.transform.parent.parent.GetChild(0).GetChild(0).name);
                        if (Col.transform.parent.parent.GetChild(0).GetChild(0).GetComponent<Cage>() != null)
                        {

                            Col.transform.parent.parent.GetChild(0).GetChild(0).GetComponent<Cage>().GetHit(1);
                            Instantiate(m_dustEffect, Col.transform.parent.parent.GetChild(0).GetChild(0).transform.position, Quaternion.identity);
                            AudioManager.Instance.PlayClipOneShot("Splat");
                            Instantiate(m_coinsEffect, Col.transform.parent.parent.GetChild(0).GetChild(0).transform.position, Quaternion.identity);

                            Debug.Log("parent.parent.sibling.sibling");

                        }

                    }

                }
            }

            
            Destroy(this.gameObject);
        }else

        if (Col.gameObject.CompareTag("Cage"))
        {

            Col.gameObject.GetComponent<Cage>().GetHit(1);
            
            Instantiate(m_dustEffect, transform.position, Quaternion.identity);

            if (isBanana)
            {
                Col.transform.parent.SetAsLastSibling();
                Instantiate(m_coinsEffect, transform.position, Quaternion.identity);
                if (Col.transform.parent.GetSiblingIndex() > 0)
                {
                    Debug.Log(Col.transform.parent.parent.name);
                    Debug.Log(Col.transform.parent.parent.GetChild(0).name);
                    if (Col.transform.parent.parent.GetChild(0).childCount > 0)
                    {

                        if(Col.transform.parent.parent.GetChild(0).GetChild(0).GetComponent<Enemy>() != null)
                        {
                            Debug.Log("parent.parent.sibling.sibling");
                            Col.transform.parent.parent.GetChild(0).GetChild(0).GetComponent<Enemy>().GetHit(1);
                            Instantiate(m_dustEffect, Col.transform.parent.parent.GetChild(0).GetChild(0).transform.position, Quaternion.identity);
                            Instantiate(m_coinsEffect, Col.transform.parent.parent.GetChild(0).GetChild(0).transform.position, Quaternion.identity);
                            AudioManager.Instance.PlayClipOneShot("Splat");
                        }
                    
                    }

                }
            }

            Destroy(this.gameObject);
        }

        if (Col.gameObject.CompareTag("Object") && !GorillaMode.Instance.isEnabled)
        {


            Timer.Instance.DeductTime(2);
            Destroy(this.gameObject);
        }

    }


}
