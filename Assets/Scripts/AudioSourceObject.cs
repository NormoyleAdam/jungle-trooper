﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioSourceObject : MonoBehaviour {

    float m_duration;
    AudioClip m_clip;

    public void Init(AudioClip clip, float duration = 0,float volume = 1,string mixerGroup = null)
    {

        if (duration <= 0)
            duration = clip.length;

        m_duration = duration;

        m_clip = clip;

        //Create component and start timer
        var source = gameObject.AddComponent<AudioSource>();

        if (!string.IsNullOrEmpty(mixerGroup))
        {
            AudioMixerGroup group = AudioManager.Instance.m_audioMixer.FindMatchingGroups(mixerGroup)[0];
            source.outputAudioMixerGroup = group;

        }

        source.PlayOneShot(clip, volume);
        StartCoroutine(KillTimer());


    }

    public IEnumerator KillTimer()
    {

        yield return new WaitForSecondsRealtime(m_duration + 1);
        DestroyObject(this.gameObject);

    }

}
