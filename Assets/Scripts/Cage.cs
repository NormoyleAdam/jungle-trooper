﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cage : MonoBehaviour
{

    int m_health = 1;
    public int m_reward;
    public int m_timeReward;
    [SerializeField] GameObject m_splatEffect;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "MonkeyBomb")
        {
            Instantiate(m_splatEffect, transform.position, Quaternion.identity);
            AudioManager.Instance.PlayClipOneShot("Splat");
            GetHit(3);

        }
    }

    public void GetHit(int d)
    {
        if (MainGame.Instance.m_isPlaying)
        {
            m_health -= d;

            if (m_health <= 0)
                Kill();
        }
    }

    void Kill()
    {
        if (!MainGame.Instance.isUnlimited)
        {
            MainGame.Instance.m_currentLevelSettings.animalsSaved++;
            MainGame.Instance.m_currentLevelSettings.AddScore(m_reward);
        }
        else
        {
            MainGame.Instance.m_currentUnlimitedLevelSettings.animalsSaved++;
            MainGame.Instance.m_currentUnlimitedLevelSettings.AddScore(m_reward);
        }



        TrophyHandler.Instance.UpdateTrophies(2);
        Timer.Instance.AddTime(m_timeReward);
        UIHandler.Instance.PlayTextAnimation("Animals");
        this.GetComponent<Animator>().Play("CageBreak");
        AudioManager.Instance.PlayClipOneShot("WoodBreak");

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name.Contains("Animals"))
            {
                transform.GetChild(i).GetComponent<SpriteRenderer>().sortingOrder = 3;
                transform.GetChild(i).GetComponent<Animal>().FreeAnimal();
                Vector3 scale = transform.GetChild(i).localScale;
                scale.x += 0.3f;
                scale.y += 0.3f;
                transform.GetChild(i).localScale = scale;
                transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
            }
            else
            {

                transform.GetChild(i).GetComponent<SpriteRenderer>().sortingOrder = 1;
            }
        }

    }

}
