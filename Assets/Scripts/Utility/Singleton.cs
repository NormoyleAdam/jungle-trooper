﻿using UnityEngine;
using System.Collections;

/*
 * Singleton base class which will find an existing instance
 * if possible, otherwise it will create a new one.
 */

namespace AMNsoftware
{

    public class Singleton<T> : Base where T : Component
    {
        private static T m_instance;

        // Test whether an instance has been created, but don't create one
        public static bool HasInstance
        {
            get
            {
                return (m_instance != null);
            }
        }

        // Lazy-creation of the instance
        public static T Instance
        {
            get
            {
                if (m_instance == null)
                {
                    // Check if an instance already exists elsewhere
                    m_instance = FindObjectOfType<T>();

                    if (m_instance == null)
                    {
                        // Make a hidden GameObject and attach to it
                        var go = new GameObject();
                        go.hideFlags = HideFlags.DontSave;
                        go.name = typeof(T).Name + " (Singleton)";
                        m_instance = go.AddComponent<T>();
                    }
                }

                return m_instance;
            }
        }
    }

}
