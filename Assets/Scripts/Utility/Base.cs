using UnityEngine;
using System;
using System.Linq;

/*
 * Base class that should be used for all non-trivial objects. Provides helpers for:
 *   - Asserting that inspector-linked variables have been set
 *   - Instantiation of objects with easy setting of parent, position, rotation and scale
 *   - Logging functions to easily format strings or simply list items and have them formatted for you
 */

public class Base : MonoBehaviour {

	public enum LogType {
		Info,
		Warning,
		Error
	}

	[HideInInspector] public RectTransform rectTransform;

	public virtual void Awake() {
		rectTransform = GetComponent<RectTransform>();
	}

	protected void AssertEditorVariablesSet(params object[] args) {
		foreach (var arg in args)
			Debug.Assert(arg != null, "An expected variable was not set in the editor");
	}

	protected T InstantiateType<T>(UnityEngine.Object original, Vector3? position = null, Quaternion? rotation = null) where T : MonoBehaviour {
		position = position ?? Vector3.zero;
		rotation = rotation ?? Quaternion.identity;

		T newObject = (Instantiate(original, position.Value, rotation.Value) as GameObject).GetComponentInChildren<T>();

		return newObject;
	}

	protected T InstantiateType<T>(UnityEngine.Object original, GameObject parent, Vector3? position = null, Quaternion? rotation = null, bool worldPositionStays = false) where T : MonoBehaviour {
		var newObject = InstantiateType<T>(original, position, rotation);

		if (parent != null)
			newObject.transform.SetParent(parent.transform, worldPositionStays);

		return newObject;
	}

	protected T InstantiateType<T>(UnityEngine.Object original, MonoBehaviour parent, Vector3? position = null, Quaternion? rotation = null, bool worldPositionStays = false) where T : MonoBehaviour {
		var newObject = InstantiateType<T>(original, position, rotation);

		if (parent != null)
			newObject.transform.SetParent(parent.transform, worldPositionStays);

		return newObject;
	}

	protected void DestroyAllChildren(GameObject parent) {
		for (int i = parent.transform.childCount - 1; i >= 0; i--) {
			var t = parent.transform.GetChild(i);
			t.SetParent(null, false);
			Destroy(t.gameObject);
		}
	}

	protected virtual bool CheckLogVerbosity(LogType logType) {
		return true;
	}

	protected string JoinedLogString(params object[] args) {
		return string.Join(", ", args.Select(x => x != null ? x.ToString() : "[null]").ToArray());
	}

	public void LogF(string format, params object[] args) {
		if (CheckLogVerbosity(LogType.Info))
			Debug.LogFormat(format, args);
	}

	public void Log(params object[] args) {
		if (CheckLogVerbosity(LogType.Info))
	 		Debug.Log(JoinedLogString(args));
	}

	public void LogWarningF(string format, params object[] args) {
		if (CheckLogVerbosity(LogType.Warning))
			Debug.LogWarningFormat(format, args);
	}

	public void LogWarning(params object[] args) {
		if (CheckLogVerbosity(LogType.Warning))
			Debug.LogWarning(JoinedLogString(args));
	}

	public void LogErrorF(string format, params object[] args) {
		if (CheckLogVerbosity(LogType.Error))
			Debug.LogErrorFormat(format, args);
	}

	public void LogError(params object[] args) {
		if (CheckLogVerbosity(LogType.Error))
			Debug.LogError(JoinedLogString(args));
	}
}
