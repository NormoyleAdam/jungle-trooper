﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AMNsoftware;
using UnityEngine.UI;

public class Tutorial :  MonoBehaviour{

    public GameObject mainWindow;
    public GameObject[] windows;
    public Button nextButton;
    public Button backButton;
    public Text title;
    int currentWindow;
    public string[] windowTitles;

	// Use this for initialization
	void Start () {

        backButton.interactable = false;
        UpdateWindows();

    }

    public void Next()
    {
        backButton.interactable = true;
        currentWindow++;

        AudioManager.Instance.PlayClipOneShot("Button");

        if (currentWindow > windows.Length-1)
        {

            //close and resume game
            MainGame.Instance.m_currentLevelSettings.FinishedTutorial();
            DataManager.SetBool("Tutorial", true);

        }
        else
        {

            UpdateWindows();
        }
    }

    public void Back()
    {
        AudioManager.Instance.PlayClipOneShot("Button");

        currentWindow--;
        if (currentWindow == 0)
        {

            backButton.interactable = false;
        }
        UpdateWindows();
    }

    void UpdateWindows()
    {
        foreach (var item in windows)
        {
            item.SetActive(false);
        }

        windows[currentWindow].SetActive(true);

        if(currentWindow == windows.Length - 1)
        {
            mainWindow.SetActive(false);
            return;
        }

        title.text = windowTitles[currentWindow];
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
