﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AMNsoftware;

public class LevelSelection : Singleton<LevelSelection> {

    public GameObject[] levelButtons;
    int currentSelectedLevel;

    public GameObject levelInfoWindow;
    public Text infoWindowTitle;
    public Image[] infoWindowStars;
    public Text highscoreAnimals;
    public Text highscoreEnemies;
    public Text nextStarAnimals;
    public Text nextStarEnemies;

    // Use this for initialization, this is only called the first time the player plays the game
    public void Init()
    {
        UpdateButtons();
    }

    void Start () {

        UpdateButtons();

	}

    public void UnlockNextLevel()
    {

        if (DataManager.Instance.saveData.unlockedLevels >= 19)
        {
            UIHandler.Instance.ShowGameCompleteScreen();
            return;
        }

        MainGame.Instance.unlockedLevelID = DataManager.Instance.saveData.unlockedLevels;
        DataManager.SetInt("unlockedLevelID", MainGame.Instance.unlockedLevelID);
        DataManager.Instance.saveData.unlockedLevels = MainGame.Instance.unlockedLevelID;
        DataManager.SaveGameData();
        Debug.Log("Unlocked Level : "+ MainGame.Instance.unlockedLevelID);
        UpdateButtons();
    }

    public void Open()
    {

        gameObject.SetActive(true);

        MainGame.Instance.UpdateLevels();

        for (int i = 0; i < levelButtons.Length; i++)
        {
            levelButtons[i].GetComponent<LevelSelectButton>().UpdateStars(DataManager.GetData().scoreData[i].stars);
        }

        UpdateButtons();

    }

    public void OpenInfoPanel(int level)
    {
        currentSelectedLevel = level;
        levelInfoWindow.SetActive(true);
        infoWindowTitle.text = DataManager.Instance.data.levelConfigurations[level - 1].levelName;
        highscoreAnimals.text = DataManager.GetData().scoreData[level - 1].animalsSaved.ToString();
        highscoreEnemies.text = DataManager.GetData().scoreData[level - 1].poachersKilled.ToString();
        nextStarAnimals.text = DataManager.Instance.data.levelConfigurations[level - 1].animalsSavedStarRequirements[Mathf.Max(DataManager.Instance.saveData.scoreData[level - 1].stars - 1, 0)].ToString();
        nextStarEnemies.text = DataManager.Instance.data.levelConfigurations[level - 1].poachersKilledStarRequirements[Mathf.Max(DataManager.Instance.saveData.scoreData[level - 1].stars - 1,0)].ToString();

        for (int i = 0; i < 3; i++)
        {

            infoWindowStars[i].color = new Color(0f,0f,0f,0.25f);

            if(DataManager.Instance.saveData.scoreData[level - 1].stars > i)
            {
                infoWindowStars[i].color = new Color(1f, 1f, 1f, 1f);
            }

        }

        AudioManager.Instance.PlayClipOneShot("UI_LowPop", 0, 1, "UI");

    }

    public void PlaySelectedLevel()
    {

        MainGame.Instance.PlayLevel(currentSelectedLevel);

    }

    public void UpdateButtons()
    {

        for (int i = 0; i <= DataManager.GetData().unlockedLevels; i++)
        {

            levelButtons[i].GetComponent<LevelSelectButton>().UnlockLevel();

        }

        MainGame.Instance.unlockedLevelID = DataManager.Instance.saveData.unlockedLevels;

    }

    public void Close()
    {

        this.gameObject.SetActive(false);

    }


}
