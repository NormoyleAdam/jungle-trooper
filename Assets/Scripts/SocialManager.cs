﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using TwitterKit.Unity;

public class SocialManager : MonoBehaviour {

	// Use this for initialization
	void Awake ()
    {
        if (!FB.IsInitialized) { FB.Init(); }

        Twitter.AwakeInit();
	}

    private void Start()
    {
        Twitter.Init();
        
    }

    public void CloseShareMenu()
    {
        MainGame.Instance._shareMenu.SetActive(false);
    }


    public void ShareGameOnFacebook()
    {
        if (FB.IsLoggedIn)
        {
            FB.ShareLink(new System.Uri("http://www.amnsoftware.co.uk/jungletrooper"),"Jungle Trooper","Get Jungle Trooper Now! for IOS & Android",new System.Uri("http://www.amnsoftware.co.uk/wp-content/uploads/2017/12/Banner.png"));
        }
        else
        {
            FB.LogInWithPublishPermissions(null, FacebookLoggedIn);
        }
    }

    public void ShareProgressOnFacebook()
    {
        if (FB.IsLoggedIn)
        {
            FB.ShareLink(new System.Uri("http://www.amnsoftware.co.uk/jungletrooper"), "Jungle Trooper", "I just scored " + MainGame.Instance.lastCoinScore.ToString() + " on Jungle Trooper!", new System.Uri("http://www.amnsoftware.co.uk/wp-content/uploads/2017/12/Banner.png"));
        }
        else
        {
            FB.LogInWithPublishPermissions(null,FacebookLoggedIn);
        }
    }

    void FacebookLoggedIn(ILoginResult result)
    {
        if(result.Error == null)
        {
            FB.ShareLink(new System.Uri("http://www.amnsoftware.co.uk/jungletrooper"), "Jungle Trooper", "Get Jungle Trooper Now! for IOS & Android", new System.Uri("http://www.amnsoftware.co.uk/wp-content/uploads/2017/12/Banner.png"));
        }
        else
        {
            UIHandler.Instance.ShowError(result.Error);
        }

    }

    void OnFacebookFailed()
    {

    }

    void OnTwitterFailed(ApiError err)
    {
        UIHandler.Instance.ShowError(err.code + " : "+err.message);
    }

    public void ShareGameOnTwitter()
    {
        if(Twitter.Session != null)
        {
            Twitter.Compose(Twitter.Session, "http://www.amnsoftware.co.uk/wp-content/uploads/2017/12/Banner.png", "Get Jungle Trooper Now! for IOS & Android");
        }
        else
        {
            Twitter.LogIn(OnTwitterLogin, OnTwitterFailed);
        }
        
    }

    public void ShareProgressOnTwitter()
    {

        if (Twitter.Session != null)
        {
            Twitter.Compose(Twitter.Session, "http://www.amnsoftware.co.uk/wp-content/uploads/2017/12/Banner.png", "I just scored " + MainGame.Instance.lastCoinScore.ToString() + " on Jungle Trooper!");
        }
        else
        {
            Twitter.LogIn(OnTwitterLogin,OnTwitterFailed);
        }
    }

    void OnTwitterLogin(TwitterSession session)
    {
        if (session != null)
        {
            Debug.Log(Twitter.Session + " : Logged in");
            Twitter.Compose(Twitter.Session, "http://www.amnsoftware.co.uk/wp-content/uploads/2017/12/Banner.png", "Get Jungle Trooper Now! for IOS & Android");
        }
    }

}
