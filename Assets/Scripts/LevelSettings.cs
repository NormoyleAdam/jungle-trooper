﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEngine.UI;public class LevelSettings : MonoBehaviour{        [SerializeField] Transform[] TrainSpawns;    [SerializeField] GameObject[] TrainCart;    [SerializeField] GameObject TrainMotor;    [SerializeField] GameObject[] m_enemyPrefab;    [SerializeField] GameObject[] m_cagePrefab;    [SerializeField] GameObject[] m_animalsPrefab;    public int levelID;    public int currentScore;    public int animalsSaved;    public int poachersKilled;    public int starsObtained;    public GameObject playerCharacter;    public float currentLevelSpeed;    public Animator introAnimator;    public Text introText;    float lastTrack;    public LevelConfig levelData;    public PlayerController pc;    public AudioSource audioSource;    float currentTrainLength;    public GameObject playerPivot;    // Use this for initialization    void Start()    {        levelData = DataManager.Instance.data.levelConfigurations[levelID - 1];        pc.ChangeWeapon(MainGame.Instance.defaultWeapon);        MainGame.Instance.currentLevelID = levelID;        pc.GetComponentInChildren<SpriteRenderer>().sprite = MainGame.Instance.playerOutfits[DataManager.GetInt("CurrentPlayerSkin")];

        MainGame.Instance.m_currentLevelSettings = this;        currentLevelSpeed = levelData.trainSpeed;        UIHandler.Instance.UpdateLevelScore(MainGame.Instance.totalCoins, poachersKilled, animalsSaved);        audioSource = gameObject.AddComponent<AudioSource>();        MainGame.Instance.ResetWeaponCoolDowns();        if(levelID == 1 || !DataManager.HasKey("Tutorial"))
        {

            UIHandler.Instance.ShowTutorial();
        }
        else
        {
            introText.text = levelData.levelName + " -";
            introText.resizeTextMinSize = 30;
           // introText.fontSize -= 12;
            introAnimator.Play("LevelIntro");

            Timer.Instance.ResetTimer();
            Timer.Instance.StartTimer();
            StartCoroutine("TrainSpawner");

        }        Vector3 pv = Vector3.one;        if (!DataManager.GetBool("PlayerIsLeft"))
        {
            pv.x = -1;
        }

        playerPivot.transform.localScale = pv;        GameObject.Find("Camera").GetComponent<AudioListener>().enabled = false;    }	    // Update is called once per frame    void Update()    {

        UIHandler.Instance.UpdateGemCount(MainGame.Instance.totalGems);        if (MainGame.Instance.m_isPlaying)        {            if(animalsSaved >= levelData.animalsSavedStarRequirements[2] && poachersKilled >= levelData.poachersKilledStarRequirements[2] && starsObtained == 2)            {                starsObtained = 3;                UIHandler.Instance.m_mainUIStars[2].Play("UIStarPopUp");                AudioManager.Instance.PlayClipOneShot("Star", 0, 1, "UI");                audioSource.PlayOneShot(MainGame.Instance.starSFX);
                UIHandler.Instance.ShowLevelComplete();            }            else if (animalsSaved >= levelData.animalsSavedStarRequirements[1] && poachersKilled >= levelData.poachersKilledStarRequirements[1] && starsObtained == 1)            {                starsObtained = 2;                UIHandler.Instance.m_mainUIStars[1].Play("UIStarPopUp");                AudioManager.Instance.PlayClipOneShot("Star", 0, 1, "UI");                audioSource.PlayOneShot(MainGame.Instance.starSFX);            }            else if (animalsSaved >= levelData.animalsSavedStarRequirements[0] && poachersKilled >= levelData.poachersKilledStarRequirements[0] && starsObtained == 0)            {                starsObtained = 1;                UIHandler.Instance.m_mainUIStars[0].Play("UIStarPopUp");                AudioManager.Instance.PlayClipOneShot("Star", 0, 1, "UI");                audioSource.PlayOneShot(MainGame.Instance.starSFX);                UIHandler.Instance.ShowLevelUnlocked();            }        }    }    public void FinishedTutorial()
    {
        UIHandler.Instance.CloseTutorial();
        introText.text = levelData.levelName;        introText.fontSize -= 12;        introAnimator.Play("LevelIntro");        Timer.Instance.ResetTimer();        Timer.Instance.StartTimer();        StartCoroutine("TrainSpawner");
    }    public void ShowEndGame()    {        MainGame.Instance.m_isPlaying = false;        DataManager.Instance.saveData.scoreData[levelID - 1].UpdateScores(starsObtained, animalsSaved, poachersKilled);        UIHandler.Instance.ShowEndGameUI(poachersKilled, animalsSaved, currentScore, starsObtained,levelData.levelID);        UIHandler.Instance.DisablePauseButton();    }    public void Continue()    {        Timer.Instance.AddTime((int)(Timer.Instance.m_timeMax / 2f));        MainGame.Instance.m_isPlaying = true;        Timer.Instance.StartTimer();    }    IEnumerator TrainSpawner()    {        var isFirst = true;        while (enabled)        {

            while (!MainGame.Instance.m_isPlaying)
            {

                yield return new WaitForEndOfFrame();
            }

            GenerateTrain();            if (isFirst)
            {
                yield return new WaitForSeconds(4.5f);
                UIHandler.Instance.EnablePauseButton();

            }            if (TrainSpawns.Length == 1)            {

                var time = (((currentTrainLength + 3) * 1.63f)) / (currentLevelSpeed - 0.1f);                if (isFirst)
                {
                    yield return new WaitForSeconds(time - 4.5f);
                }
                else
                {
                    yield return new WaitForSeconds(time);

                }
            }            else            {                yield return new WaitForSeconds(levelData.timeTilSpawn);            }            currentLevelSpeed += 0.1f;            isFirst = false;
        }    }    public void MissedPoacher()    {        Timer.Instance.DeductTime(2);    }    public void AddScore(int score)    {        if (MainGame.Instance.m_isPlaying)        {            currentScore += score;            MainGame.Instance.totalCoins += score;            UIHandler.Instance.PlayTextAnimation("Coins");            UIHandler.Instance.UpdateLevelScore(MainGame.Instance.totalCoins, poachersKilled, animalsSaved);            TrophyHandler.Instance.UpdateTrophies(3, score);        }    }    public void GenerateTrain()    {        GameObject NewTrain = new GameObject();        NewTrain.name = "TrainObject";        float track = Random.Range(0, TrainSpawns.Length);               bool isNewTrack = false;        if (TrainSpawns.Length > 1)        {            while (!isNewTrack)            {                if (track == lastTrack)                {                    track = Random.Range(0, TrainSpawns.Length);                    isNewTrack = false;                }                else                {                    isNewTrack = true;                    lastTrack = track;                }            }        }        else        {            track = 0;        }        var numberOfCarts = Random.Range(levelData.minTrainLength, levelData.maxTrainLength + 1);        var spawnPos = TrainSpawns[(int)track].position;        spawnPos.x -= 4;        NewTrain.transform.position = spawnPos;        currentTrainLength = numberOfCarts;        for (int i = 0; i < numberOfCarts; i++)        {            GameObject cart;
            Vector3 pos;

            if (track == 1)
            {
                cart = (GameObject)Instantiate(TrainCart[0], NewTrain.transform.position, Quaternion.identity, NewTrain.transform);
            }
            else
            {
                cart = (GameObject)Instantiate(TrainCart[Random.Range(0, TrainCart.Length)], NewTrain.transform.position, Quaternion.identity, NewTrain.transform);
            }

            pos = cart.transform.position;
            pos.x -= 1.63f * i;
            cart.transform.position = pos;            var r = Random.Range(0, m_animalsPrefab.Length);            if(cart.transform.childCount == 2)
            {
                GameObject cage;
                GameObject animal;

                if (Random.Range(0,2) == 0)
                {

                    cage = Instantiate(m_cagePrefab[r], cart.transform.GetChild(0).transform.position, Quaternion.identity, cart.transform.GetChild(0).transform);
                    cage.transform.localPosition = new Vector3(0, 0.065f, 0);
                    animal = Instantiate(m_animalsPrefab[r], cage.transform.position, Quaternion.identity, cage.transform);

                    if (track == 0)
                    {

                        cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        cart.GetComponent<SpriteRenderer>().sortingOrder = 0;
                        animal.transform.SetAsLastSibling();

                        cage.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        cage.GetComponent<SpriteRenderer>().sortingOrder = 3;
                        cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 1;


                        animal.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        animal.GetComponent<SpriteRenderer>().sortingOrder = 2;

                    }
                    else
                    {

                        cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                        animal.transform.SetAsLastSibling();
                        cage.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        cage.GetComponent<SpriteRenderer>().sortingOrder = 3;
                        cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 1;


                        animal.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        animal.GetComponent<SpriteRenderer>().sortingOrder = 2;

                    }

                }
                else
                {
                    cage = Instantiate(m_enemyPrefab[Random.Range(0, m_enemyPrefab.Length)], cart.transform.GetChild(0).transform.position, Quaternion.identity, cart.transform.GetChild(0).transform);
                    cage.transform.localPosition = new Vector3(0, 0.25f, 0);
                    if (track == 0)
                    {

                        cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                        cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                        cage.GetComponent<Enemy>().SetSortingLayerName("Train1");


                    }
                    else
                    {

                        cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                        cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                        cage.GetComponent<Enemy>().SetSortingLayerName("Train2");


                    }
                }

                
            }
            else
            {
                var cage = Instantiate(m_cagePrefab[r], cart.transform.GetChild(1).transform.position, Quaternion.identity, cart.transform.GetChild(1).transform);
                var animal = Instantiate(m_animalsPrefab[r], cage.transform.position, Quaternion.identity, cage.transform);
                var enemy = Instantiate(m_enemyPrefab[Random.Range(0, m_enemyPrefab.Length)], cart.transform.GetChild(0).transform.position, Quaternion.identity, cart.transform.GetChild(0).transform);

                if (track == 0)
                {

                    cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                    cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                    animal.transform.SetAsLastSibling();
                    cage.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                    cage.GetComponentInChildren<SpriteRenderer>().sortingOrder = 3;
                    cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                    cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 1;


                    animal.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";
                    animal.GetComponent<SpriteRenderer>().sortingOrder = 2;

                    enemy.GetComponent<Enemy>().SetSortingLayerName("Train1");
                    //enemy.GetComponent<SpriteRenderer>().sortingOrder = 4;

                }
                else
                {

                    cart.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                    cart.GetComponent<SpriteRenderer>().sortingOrder = 0;

                    cage.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                    cage.GetComponentInChildren<SpriteRenderer>().sortingOrder = 3;
                    cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                    cage.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 1;

                    animal.transform.SetAsLastSibling();

                    animal.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";
                    animal.GetComponent<SpriteRenderer>().sortingOrder = 2;

                    enemy.GetComponent<Enemy>().SetSortingLayerName("Train2");



                }

            }                    }        //Generate Motor        var Motor = (GameObject)Instantiate(TrainMotor, NewTrain.transform.position, Quaternion.identity, NewTrain.transform);        if (track == 0)        {            Motor.GetComponent<SpriteRenderer>().sortingLayerName = "Train1";            Motor.GetComponent<SpriteRenderer>().sortingOrder = 0;            Motor.GetComponent<TrainMotor>().Speed = currentLevelSpeed;        }        else        {            Motor.GetComponent<SpriteRenderer>().sortingLayerName = "Train2";            Motor.GetComponent<SpriteRenderer>().sortingOrder = 0;            Motor.GetComponent<TrainMotor>().Speed = currentLevelSpeed;        }        //1f + (0.1f * track * levelID) ;    }}