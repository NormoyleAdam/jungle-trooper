﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WeaponSelectionItem : MonoBehaviour {

    public WeaponType weapon;
    public float coolDownTime;
    public int value;
    bool isCool = true;
    public GameObject objectPrefab;
    PlayerController pc;

    public Image coolDownImage;

    void SetValues()
    {

        switch (weapon)
        {

            case WeaponType.Banana:
                value = MainGame.Instance.bananaPeelAmount;
                break;
            case WeaponType.Bomb:
                value = MainGame.Instance.monkeyBombAmount;
                break;
            case WeaponType.Lightning:
                value = MainGame.Instance.lightningModeAmount;
                break;
            case WeaponType.SlowMo:
                value = MainGame.Instance.slowMoAmount;
                break;
            case WeaponType.Stars:
                value = 1;
                break;

        }
    }

    void UpdateValues()
    {

        switch (weapon)
        {

            case WeaponType.Banana:
                 MainGame.Instance.bananaPeelAmount = value;
                break;
            case WeaponType.Bomb:
                MainGame.Instance.monkeyBombAmount = value;
                break;
            case WeaponType.Lightning:
                MainGame.Instance.lightningModeAmount = value;
                break;
            case WeaponType.SlowMo:
                MainGame.Instance.slowMoAmount = value;
                break;
            case WeaponType.Stars:
                value = 1;
                break;

        }

    }

    public void Deduct()
    {

        value--;
        UpdateValues();
        MainGame.Instance.UpdateWeaponValues();

    }

    public bool CheckValue()
    {

        SetValues();


        if (value > 0 && isCool && MainGame.Instance.m_isPlaying)
        {
            StartCoroutine("CoolDown");
            return true;

        }
        else
        {
            return false;
        }

    }

    public void CoolDownTimerReset()
    {

        StopAllCoroutines();
        isCool = true;
        coolDownImage.fillAmount = 0;
    }

    IEnumerator CoolDown()
    {
        isCool = false;
        float x = coolDownTime;

        while (!isCool)
        {

            yield return new WaitForSeconds(0.01f);

            x -= 0.01f;

            coolDownImage.fillAmount = (float)x / coolDownTime;

            if(x <= 0)
            {
                isCool = true;

            }


        }


    }

    public void ResetWeapon()
    {
        switch (weapon)
        {
            case WeaponType.SlowMo:
                    GorillaMode.Instance.StopGorillaMode();
                break;
            case WeaponType.Lightning:
                    EnergyBoost.Instance.DeactivateEnergyBoost();
                break;
        }
    }

    public void UseWeapon()
    {

        SetValues();

        AudioManager.Instance.PlayClipOneShot("Button");

        if (value > 0)
        {

            if (MainGame.Instance.isUnlimited)
            {
                pc = MainGame.Instance.m_currentUnlimitedLevelSettings.pc;
            }
            else
            {
                pc = MainGame.Instance.m_currentLevelSettings.pc;
            }

            switch (weapon)
            {

                case WeaponType.Banana:
                    pc.ChangeWeapon(this);
                    break;
                case WeaponType.Bomb:
                    if (CheckValue())
                    {
                        Quaternion q = new Quaternion();
                        q = Quaternion.Euler(0, 0, 70f);
                        Instantiate(objectPrefab, Vector3.zero, q);
                        Deduct();
                    }
                    break;
                case WeaponType.SlowMo:
                    if (CheckValue())
                    {
                        GorillaMode.Instance.StartGorillaMode();
                        Deduct();
                    }
                    break;
                case WeaponType.Lightning:
                    if (CheckValue())
                    {
                        EnergyBoost.Instance.ActivateEnergyBoost();
                        Deduct();
                    }
                    break;
                case WeaponType.Stars:
                    pc.ChangeWeapon(this);
                    break;

            }


        }
    }




}

public enum WeaponType
{

    Banana,
    Bomb,
    SlowMo,
    Lightning,
    Stars
}
