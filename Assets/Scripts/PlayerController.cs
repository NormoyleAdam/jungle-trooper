﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{


    GameObject thrownObject;
    AudioSource audio;
    [SerializeField] AudioClip[] throwClips;
    [SerializeField] GameObject linerender;

    bool tracking = false;
    bool isDragging = false;

    Vector3 lastmousepos;
    List<Vector3> postions = new List<Vector3>();
    Vector3 initPos;

    public WeaponSelectionItem currentWeapon;

    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        currentWeapon = MainGame.Instance.defaultWeapon;
        thrownObject = currentWeapon.objectPrefab;
    }
	
    // Update is called once per frame
    void Update()
    {
		
        GetInput();

    }

    public void ChangeWeapon(WeaponSelectionItem w)
    {

        currentWeapon = w;
        thrownObject = currentWeapon.objectPrefab;

    }

    void InitializeThrow()
    {

        linerender.SetActive(true);
        linerender.GetComponent<TrailRenderer>().sortingLayerName = "FG";
        linerender.GetComponent<TrailRenderer>().sortingOrder = 1000;
        tracking = true;
        isDragging = true;
        initPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        postions.Clear();

    }

    void GetInput()
    {

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && MainGame.Instance.m_isPlaying && Input.mousePosition.y < (Screen.height/3))
        {

            Debug.Log(Input.mousePosition.y);

            if (currentWeapon.CheckValue())
            {


                InitializeThrow();

            }
            else
            {

                currentWeapon = MainGame.Instance.defaultWeapon;
                thrownObject = currentWeapon.objectPrefab;

            }



        }

        if (Input.GetMouseButton(0) && tracking)
        {
            var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;
            linerender.transform.position = pos;
            lastmousepos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            lastmousepos.z = 0;
            postions.Add(lastmousepos);

        }

        if (Input.GetMouseButtonUp(0) && MainGame.Instance.m_isPlaying)
        {

            if (isDragging)
            {
               
                isDragging = false;

                var point1 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                point1.z = 0;

                if (Vector3.Distance(postions[0], point1) > 0.2f && postions[0].y < point1.y)
                {

                    var c = postions.Count - 1;

                    var force = Vector3.Normalize(postions[c] - postions[0]);
                    force = force * 5;

                    if (force.y > 0.3f)
                    {
                        initPos.z = 0;
                        var banana = (GameObject)Instantiate(thrownObject, initPos, Quaternion.identity);
                        banana.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
                        audio.PlayOneShot(throwClips[Random.Range(0, throwClips.Length)]);
                        MainGame.Instance.UpdateWeaponValues();
                        currentWeapon.Deduct();
                        
                    }


                    //var force = Vector3.Normalize(point - initPos) * (20 * Vector3.Distance(point, point1));
                    //force.y = Mathf.Clamp(force.y, 2, 6);
                    //force.x = Mathf.Clamp(force.x, -6, 6);


                }

            }

            linerender.SetActive(false);

            tracking = false;
        }


    }
}
