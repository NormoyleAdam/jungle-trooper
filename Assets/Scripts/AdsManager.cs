﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;

using AMNsoftware;

public class AdsManager : SingletonPersistent<AdsManager>
{
    bool isAllowedToShowAd = false;
    public int adTimerDuration = 150; //2.5 mins
    ShowAdPlacementContent ad;

    public void ShowAd()
    {
        if (isAllowedToShowAd)
        {
            ad = Monetization.GetPlacementContent("video") as ShowAdPlacementContent;
          
            ad.Show();
            isAllowedToShowAd = false;
        }


    }

    public override void Awake()
    {
        base.Awake();

        Monetization.Initialize("ba40904b-4917-4df5-b10a-b46b5cf8810d", false);

        StartCoroutine(AdTimer());
    }

    //This will only allow add every X secconds
    IEnumerator AdTimer()
    {
        while (Application.isPlaying)
        {
            yield return new WaitForSecondsRealtime(adTimerDuration);
            isAllowedToShowAd = !MainGame.Instance.isPaidVersion;

        }

    }

    public void ShowAdToContinue(ShowResult result)
    {

        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                MainGame.Instance.ContinueLevel();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                MainGame.Instance.BackToLevelSelection();
                UIHandler.Instance.CloseEndGameWindow();
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                MainGame.Instance.BackToLevelSelection();
                UIHandler.Instance.CloseEndGameWindow();
                break;
        }

    }

    public void ShowAdForGem(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }

    }

    public void ShowStartGameAd(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");

                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }

        MainGame.Instance.GameStart();
        //UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(0);


    }

    public void WatchAddToContinue()
    {
        isAllowedToShowAd = true;
        StartCoroutine("ShowRewardedAd","EndGameContinue");
    }

    public void ShowAdOverride(string condition = null)
    {
        isAllowedToShowAd = true;
        if(condition == null)
        {
            ad = Monetization.GetPlacementContent("rewardedVideo") as ShowAdPlacementContent;
            ad.Show();
        }
        else
        {
            StartCoroutine(ShowRewardedAd(condition));
        }
    }

    public IEnumerator ShowRewardedAd(string condition)
    {

        ad = Monetization.GetPlacementContent("rewardedVideo") as ShowAdPlacementContent;

        while (!ad.ready)
        {
            yield return new WaitForEndOfFrame();
        }


        ShowAdFinishCallback options = HandleShowResult;
        switch (condition)
        {
            case "EndGameContinue":
                options = ShowAdToContinue;
                break;
            case "FreeGem":
                options =  ShowAdForGem ;
                break;
            case "GameStart":
                options = ShowStartGameAd ;
                break;
        }

        ad.Show(options);
        isAllowedToShowAd = false;
        
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
