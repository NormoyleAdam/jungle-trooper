﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AMNsoftware;

public class EnergyBoost : Singleton<EnergyBoost> {

    public bool isEnabled;
    public float timeChangeAmount;

    [SerializeField] GameObject timerObject;
    [SerializeField] GameObject lightningObject;
    [SerializeField] Text timerText;
    [SerializeField] AudioClip soundEffect;


    public void ActivateEnergyBoost()
    {
        lightningObject.SetActive(true);
        isEnabled = true;
        //overlay.enabled = true;
        timerObject.SetActive(true);

        AudioManager.Instance.PlayClipOneShot(soundEffect.name);
        StartCoroutine("TimerTick");
    }

    public void DeactivateEnergyBoost()
    {
        StopAllCoroutines();
        lightningObject.SetActive(false);
        isEnabled = false;
        //overlay.enabled = false;
        timerObject.SetActive(false);
        Time.timeScale = 1f;
    }

    IEnumerator TimerTick()
    {

        var t = 5f;

        while (isEnabled)
        {

            while (MainGame.Instance.m_isPlaying)
            {

                t -= 0.1f;

                timerText.text = ((int)t).ToString() + "s";

                //Time.timeScale = Mathf.Lerp(Time.timeScale, 0.5f, 0.1f);

                yield return new WaitForSecondsRealtime(0.1f);


                if (t <1)
                {
                    MainGame.Instance.ResetWeaponCoolDowns(true);
                    DeactivateEnergyBoost();
                }

            }

            yield return new WaitForEndOfFrame();

        }

    }
}
