﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpPanel : MonoBehaviour {

    [SerializeField] GameObject m_pageParent;
    int m_currentPage = 0;
    List<GameObject> m_pages = new List<GameObject>();

    // Use this for initialization
    void Start () {

        for(int i = 0; i <= m_pageParent.transform.childCount - 1; i++)
        {

            m_pages.Add(m_pageParent.transform.GetChild(i).gameObject);

        }


    }

    public void NextPage()
    {

        m_pages[m_currentPage].SetActive(false);
        m_currentPage++;
        m_pages[m_currentPage].SetActive(true);
    }

    public void PrevPage()
    {

        m_pages[m_currentPage].SetActive(false);
        m_currentPage--;
        m_pages[m_currentPage].SetActive(true);

    }
}
